import { combineReducers } from "redux";

import {
  statefulSetsReducer,
  persistentVolumesReducer,
  deploymentsReducer,
  daemonSetsReducer,
  configMapsReducer,
  V1CertificateReducer,
  servicesReducer,
  serviceAccountsReducer
} from "@opstrace/kubernetes";

export const rootReducers = {
  kubernetes: combineReducers({
    cluster: combineReducers({
      StatefulSets: statefulSetsReducer,
      Deployments: deploymentsReducer,
      DaemonSets: daemonSetsReducer,
      PersistentVolumes: persistentVolumesReducer,
      ConfigMaps: configMapsReducer,
      Certificates: V1CertificateReducer,
      Services: servicesReducer,
      ServiceAccounts: serviceAccountsReducer
    })
  })
};

export const rootReducer = combineReducers(rootReducers);
export type State = ReturnType<typeof rootReducer>;
