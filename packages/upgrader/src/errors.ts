export class ClusterUpgradeTimeoutError extends Error {
  constructor(...args: string[] | undefined[]) {
    super(...args);
    Error.captureStackTrace(this, ClusterUpgradeTimeoutError);
  }
}
