module.exports = {
  schema: ["./graphql.schema.json"],
  documents: ["**/*.{gql}"],
  extensions: {
    endpoints: {
      default: {
        url: "http://localhost:8080/v1/graphql",
        headers: {
          "X-Hasura-Admin-Secret": process.env.HASURA_GRAPHQL_ADMIN_SECRET
        }
      }
    }
  }
};
