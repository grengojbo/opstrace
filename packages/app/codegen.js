const endpoint = process.env.HASURA_GRAPHQL_ENDPOINT || "http://localhost:8080";
const adminSecret = process.env.HASURA_GRAPHQL_ADMIN_SECRET;

module.exports = {
  documents: "./src/**/*.gql",
  schema: [
    {
      [endpoint + "/v1/graphql"]: {
        headers: {
          "X-Hasura-Admin-Secret": adminSecret
        }
      }
    }
  ],
  overwrite: true,
  generates: {
    "./src/state/graphql-api-types.ts": {
      plugins: [
        "typescript",
        "typescript-operations",
        "typescript-graphql-request"
      ],
      config: {
        rawRequest: true,
        skipTypename: true
      },
      hooks: {
        afterOneFileWrite: ["yarn run prettier --write"]
      }
    },
    "../controller/src/dbSDK.ts": {
      plugins: [
        "typescript",
        "typescript-operations",
        "typescript-graphql-request"
      ],
      config: {
        rawRequest: true,
        skipTypename: true
      },
      hooks: {
        afterOneFileWrite: ["yarn run prettier --write"]
      }
    },
    "../../go/pkg/graphql/client_generated.go": {
      plugins: ["graphql-codegen-golang"],
      config: {
        packageName: "graphql"
      },
      hooks: {
        afterOneFileWrite: ["go fmt"]
      }
    }
  }
};
