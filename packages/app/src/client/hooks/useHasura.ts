import useSWR from "swr";

import { client } from "state/clients/graphqlClient";

import useDeepMemo from "client/hooks/useDeepMemo";

// NTW: SWR does shallow comparisons of the token passed to determine cache hits/misses
// passing in structured graphql variables means we never get a cache hit, so have
// wrapped the useSWR call with useDeepMemo to ensure that the same array is passed in
// for the same variables

const useHasura = (query: string, variables?: {}) => {
  const token = useDeepMemo(() => [query, variables], [query, variables]);

  return useSWR(token, fetcher);
};

const fetcher = (query: string, variables?: {}) =>
  client.request(query, variables);

export default useHasura;
