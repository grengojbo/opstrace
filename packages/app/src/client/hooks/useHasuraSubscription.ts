import useSWR, { Key, mutate, SWRResponse } from "swr";

import useDeepMemo from "client/hooks/useDeepMemo";

import {
  endpoint,
  headers as authHeader
} from "state/clients/graphqlClient/subscriptionClient";

const headers = { "Content-Type": "application/json", ...authHeader };

const useHasuraSubscription = (
  query: string,
  variables: {} = {}
): SWRResponse<void, any> => {
  const token = useDeepMemo(() => [query, variables], [query, variables]);
  return useSWR(token, fetcher(token, query, variables));
};

const fetcher = (token: Key, query: string, variables: {} = {}) => {
  let latestData = null;

  const subscribe = async (query: string, variables: {} = {}) => {
    if (typeof window !== "undefined") {
      const ws = new WebSocket(endpoint, "graphql-ws");
      const init_msg = {
        type: "connection_init",
        payload: { headers }
      };
      ws.onopen = function (event) {
        ws.send(JSON.stringify(init_msg));
        const msg = {
          id: "1",
          type: "start",
          payload: {
            variables: variables,
            extensions: {},
            operationName: null,
            query: query
          }
        };
        ws.send(JSON.stringify(msg));
      };
      ws.onmessage = function (data) {
        const finalData = JSON.parse(data.data);
        if (finalData.type === "data") {
          latestData = finalData.payload.data;
          mutate(token, latestData, false);
          return latestData;
        }
      };
    }
  };

  return () => subscribe(query, variables);
};

export default useHasuraSubscription;
