import useSWR from "swr";
import axios from "axios";

import { grafanaUrl } from "client/utils/grafana";

export const useLoki = (path: string, tenantName: string = "system") => {
  const url = `${grafanaUrl({
    tenant: tenantName
  })}/grafana/api/datasources/proxy/2/loki/api/v1/${path}`;

  return useSWR(url, fetcher);
};

export const usePrometheus = (path: string, tenantName: string = "system") => {
  const url = `${grafanaUrl({
    tenant: tenantName
  })}/grafana/api/datasources/proxy/1/api/v1/${path}`;

  return useSWR(url, fetcher);
};

const fetcher = (url: string) =>
  axios({
    method: "get",
    url: url,
    withCredentials: true
  }).then(res => res.data);
