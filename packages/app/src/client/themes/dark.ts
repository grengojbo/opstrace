import deepmerge from "deepmerge";
import { createMuiTheme } from "@material-ui/core/styles";
import * as colors from "@material-ui/core/colors";

import { PaletteType } from "./types";
import common from "./common";

export const theme = {
  name: "dark",
  palette: {
    type: "dark" as PaletteType,
    background: {
      default: "#181C24",
      paper: "#181C24"
    },
    divider: colors.grey[700],
    info: {
      main: colors.blueGrey[200],
      contrastText: colors.common.white
    }
  }
  // overrides: {
  //   MuiBackdrop: {
  //     root: {
  //       backgroundColor: "rgba(255, 255, 255, 0.1)"
  //     }
  //   }
  // }
};

export default createMuiTheme(deepmerge(common, theme));
