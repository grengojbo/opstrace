import React from "react";

import { Box } from "client/components/Box";
import Typography from "client/components/Typography/Typography";
import { Tabs } from "client/components/Tabs";

import CortexConfig from "./cortex";
import OpstraceConfig from "./opstrace";

const ClusterConfig = () => {
  return (
    <>
      <Box pt={1} pb={4}>
        <Typography variant="h1">Configuration</Typography>
      </Box>
      <Tabs
        tabs={[
          {
            path: `/cluster/configuration/cortex`,
            title: "Cortex",
            component: CortexConfig
          },
          {
            path: `/cluster/configuration/opstrace`,
            title: "Opstrace",
            component: OpstraceConfig
          }
        ]}
      />
    </>
  );
};

export default ClusterConfig;
