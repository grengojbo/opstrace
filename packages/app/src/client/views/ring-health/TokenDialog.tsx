import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from "client/components/Dialog";
import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { Button } from "client/components/Button";
import Grid from "@material-ui/core/Grid";

type Props = {
  tokens?: Array<number>;
  onClose: () => void;
};

const TokenDialog = ({ tokens, onClose }: Props) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <Dialog
      fullScreen={fullScreen}
      open={!!tokens}
      onClose={onClose}
      aria-labelledby="token-dialog"
    >
      <DialogTitle id="token-dialog">Tokens</DialogTitle>
      <DialogContent>
        <Grid container spacing={3}>
          {tokens?.map(token => (
            <Grid key={token} item xs={12} sm={6} md={4}>
              {token}
            </Grid>
          ))}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default TokenDialog;
