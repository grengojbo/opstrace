import { Shard } from "./RingTable";

// shard to be used as mock request response
export const createMockShard = (id: string): Required<Shard> => ({
  id: `shard-id-${id}`,
  state: `shard-state-${id}`,
  timestamp: "2021-08-04 05:16:17 +0000 UTC",
  zone: `shard-zone-${id}`,
  address: `shard-address-${id}`,
  tokens: [],
  registered_timestamp: `shard-registered_timestamp-${id}`
});
