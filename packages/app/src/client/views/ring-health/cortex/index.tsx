import React from "react";
import RingHealth from "../RingHealth";
import { Tab } from "../RingHealth";

export const TABS: Tab[] = [
  {
    title: "Ingester",
    path: `/ingester`,
    endpoint: "/_/cortex/ingester/ring"
  },
  {
    title: "Ruler",
    path: `/ruler`,
    endpoint: "/_/cortex/ruler/ring"
  },
  {
    title: "Compactor",
    path: `/compactor`,
    endpoint: "/_/cortex/compactor/ring"
  },
  {
    title: "Store-gateway",
    path: `/store-gateway`,
    endpoint: "/_/cortex/store-gateway/ring"
  },
  {
    title: "Alertmanager",
    path: `/alert-manager`,
    endpoint: "/_/cortex/alertmanager/ring"
  }
];

type Props = {
  baseUrl: string;
};

const CortexRingHealth = ({ baseUrl }: Props) => {
  const tabs = TABS.map(tab => ({ ...tab, path: baseUrl + tab.path }));

  return <RingHealth title="Cortex Ring Health" tabs={tabs} />;
};

export default CortexRingHealth;
