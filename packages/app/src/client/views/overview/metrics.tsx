import React from "react";
import Grid from "@material-ui/core/Grid";

import { Box } from "client/components/Box";
import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import GrafanaIframe from "client/components/Grafana/Iframe";

const TenantOverviewMetrics = () => {
  const tenant = useSelectedTenantWithFallback();

  return (
    <Box pt={3}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <GrafanaIframe
            initialHeight={1200}
            tenant="system"
            title="Tenant Overview: Metrics (Cortex)"
            path="/d/c82shqQZz/tenant-overview-cortex"
            params={{
              "var-tenant": tenant.name
            }}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default TenantOverviewMetrics;
