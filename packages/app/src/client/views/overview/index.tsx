import React from "react";

import { Box } from "client/components/Box";
import Typography from "client/components/Typography/Typography";
import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import { Tabs } from "client/components/Tabs";

import Metrics from "./metrics";

const TenantOverview = () => {
  const tenant = useSelectedTenantWithFallback();

  return (
    <>
      <Box pt={1} pb={4}>
        <Typography variant="h1">Tenant Overview</Typography>
      </Box>
      <Tabs
        tabs={[
          {
            path: `/tenant/${tenant.name}/overview/metrics`,
            title: "Metrics",
            component: Metrics
          }
        ]}
      />
    </>
  );
};

export default TenantOverview;
