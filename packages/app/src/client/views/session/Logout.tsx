import React, { useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import useAxios from "axios-hooks";

import { loginUrl } from "client/components/withSession/paths";

import { LoadingPage } from "./Loading";

export const LogoutPage = () => {
  const { logout } = useAuth0();
  const [{ loading }] = useAxios({
    url: "/_/auth/logout",
    method: "POST",
    withCredentials: true
  });

  useEffect(() => {
    if (!loading) {
      logout({
        returnTo: loginUrl()
      });
    }
  }, [loading, logout]);

  return <LoadingPage stage="logout" />;
};
