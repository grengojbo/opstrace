import React from "react";
import { InstalledIntegrations } from "./Installed";
import "@testing-library/jest-dom";
import { screen } from "@testing-library/react";
import { createMainStore } from "state/store";
import { updateIntegrations } from "state/integration/actions";
import uniqueId from "lodash/uniqueId";
import integrationsDefs from "client/integrations";
import randomNumber from "lodash/random";
import { renderWithEnv } from "client/utils/testutils";

const getRandomIntegrationDef = () =>
  integrationsDefs[randomNumber(0, integrationsDefs.length - 1)];

const createMockIntegration = (integrationDef = getRandomIntegrationDef()) => {
  return {
    id: `integrationID-${uniqueId()}`,
    tenant_id: `tenant_id-${uniqueId()}`,
    name: `name-${uniqueId()}`,
    key: `key-${uniqueId()}`,
    kind: integrationDef.kind,
    data: {
      deployNamespace: `data.deployNamespace-${uniqueId()}`
    },
    created_at: "2021-08-06T13:03:49.979677",
    updated_at: "2021-08-06T13:03:49.979677"
  };
};

test("renders integrations", async () => {
  const store = createMainStore();
  const integrationDef = getRandomIntegrationDef();
  const integration = createMockIntegration(integrationDef);

  store.dispatch(updateIntegrations([integration]));

  renderWithEnv(<InstalledIntegrations />, { store });

  expect(
    screen.getByRole("cell", { name: integration.name })
  ).toBeInTheDocument();
  expect(
    screen.getByRole("cell", { name: integrationDef.label })
  ).toBeInTheDocument();
});

test.todo("renders integration's status");
