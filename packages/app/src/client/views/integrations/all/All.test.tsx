import React from "react";
import { AllIntegrations } from "./All";
import "@testing-library/jest-dom";
import { screen, within } from "@testing-library/react";
import { createMemoryHistory } from "history";
import integrationsDefs from "client/integrations";
import { renderWithEnv, userEvent } from "client/utils/testutils";

test("renders integrations", async () => {
  renderWithEnv(<AllIntegrations />);
  integrationsDefs
    .filter(i9n => i9n.enabled)
    .forEach(i9n => {
      expect(screen.getByText(i9n.label)).toBeInTheDocument();
    });
});

test("does not render disabled integrations", async () => {
  renderWithEnv(<AllIntegrations />);
  integrationsDefs
    .filter(i9n => !i9n.enabled)
    .forEach(i9n => {
      expect(screen.queryByText(i9n.label)).not.toBeInTheDocument();
    });
});

test("clicking install redirects correctly", async () => {
  const history = createMemoryHistory();
  const integration = integrationsDefs[0].kind;
  renderWithEnv(<AllIntegrations />, { history });
  const integrationCard = within(screen.getByTestId(`${integration}-card`));
  userEvent.click(integrationCard.getByRole("button", { name: "Install" }));
  expect(history.location.pathname).toBe(
    `/tenant/system/integrations/all/install/${integration}`
  );
});
