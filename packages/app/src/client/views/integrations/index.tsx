import React from "react";

import { AllIntegrations } from "./all/All";
import { InstalledIntegrations } from "./installed/Installed";

import { Box } from "client/components/Box";
import Typography from "client/components/Typography/Typography";
import { Tabs } from "client/components/Tabs";
import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import { useIntegrationList } from "state/integration/hooks/useIntegrationList";

export { InstallIntegration } from "./all/Install";
export { ShowIntegration } from "./all/Show";
export { EditIntegration } from "./installed/Edit";

export const TenantIntegrations = () => {
  const tenant = useSelectedTenantWithFallback();
  const integrations = useIntegrationList();

  return (
    <>
      <Box pt={1} pb={4}>
        <Typography variant="h1">Integrations</Typography>
      </Box>
      <Tabs
        tabs={[
          {
            path: `/tenant/:tenantId/integrations/all`,
            to: `/tenant/${tenant.name}/integrations/all`,
            title: "All Integrations",
            component: AllIntegrations
          },
          {
            path: `/tenant/:tenantId/integrations/installed`,
            to: `/tenant/${tenant.name}/integrations/installed`,
            title: `Installed Integrations (${integrations.length})`,
            component: InstalledIntegrations
          }
        ]}
      />
    </>
  );
};
