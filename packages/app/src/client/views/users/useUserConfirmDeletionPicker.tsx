import { usePickerService } from "client/services/Picker";
import { useDispatch } from "react-redux";
import { deleteUser } from "state/user/actions";
import { User } from "state/user/types";

const useUserConfirmDeletionPicker = (user?: User) => {
  const dispatch = useDispatch();
  return usePickerService(
    {
      title: `Delete ${user?.email}?`,
      activationPrefix: "delete user?:",
      disableFilter: true,
      disableInput: true,
      options: [
        {
          id: "yes",
          text: "yes"
        },
        {
          id: "no",
          text: "no"
        }
      ],
      onSelected: option => {
        if (option.id === "yes" && user) dispatch(deleteUser(user.id));
      }
    },
    [user]
  );
};

export default useUserConfirmDeletionPicker;
