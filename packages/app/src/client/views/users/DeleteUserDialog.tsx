import React, { useState } from "react";

import useUserList from "state/user/hooks/useUserList";
import { usePickerService, PickerOption } from "client/services/Picker";
import { useCommandService } from "client/services/Command";
import { userToPickerOption } from "./UserPicker";
import useUserConfirmDeletionPicker from "./useUserConfirmDeletionPicker";
import { User } from "state/user/types";

export const deleteUserCommand = "delete-user-picker";

const DeleteUserPicker = () => {
  const users = useUserList();
  const [user, setSelectedUser] = useState<PickerOption<User> | null>();
  const { activatePickerWithText } = useUserConfirmDeletionPicker(user?.data);

  usePickerService(
    {
      title: "Enter user's email",
      activationPrefix: "delete user:",
      options: users ? users.map(userToPickerOption) : [],
      onSelected: option => {
        setSelectedUser(option);
        activatePickerWithText("delete user?: ");
      }
    },
    [users, activatePickerWithText]
  );

  useCommandService(
    {
      id: deleteUserCommand,
      description: "Delete User",
      disabled: users.length < 2,
      handler: e => {
        e.keyboardEvent?.preventDefault();
        activatePickerWithText("delete user: ");
      }
    },
    [users.length]
  );

  return null;
};

export default React.memo(DeleteUserPicker);
