import React from "react";
import Grid from "@material-ui/core/Grid";

import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import { grafanaUrl } from "client/utils/grafana";

import { Box } from "client/components/Box";
import { Card, CardContent, CardHeader } from "client/components/Card";
import { Typography } from "client/components/Typography";
import { ExternalLink } from "client/components/Link";

const Dashboards = () => {
  const tenant = useSelectedTenantWithFallback();

  return (
    <>
      <Box pt={1} pb={4}>
        <Typography variant="h1">Dashboards</Typography>
      </Box>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              titleTypographyProps={{ variant: "h6" }}
              title="View Dashboards"
            />
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Typography color="textSecondary" variant="body2">
                    View Dashboards for the <strong>{tenant.name}</strong>{" "}
                    tenant in Grafana.
                  </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box display="flex" justifyContent="flex-end">
                    <ExternalLink
                      href={`${grafanaUrl({
                        tenant: tenant.name
                      })}/grafana/dashboards`}
                    >
                      View Dashboards →
                    </ExternalLink>
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  );
};

export default Dashboards;
