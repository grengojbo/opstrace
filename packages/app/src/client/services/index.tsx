import React from "react";
import { InitSubscriptions } from "state/clients/graphqlClient/initSubscriptions";
import { CommandService } from "./Command";
import { PickerService } from "./Picker";
import { NotificationService } from "./Notification";
import { DisplayService } from "./Display";
import { ThemeCommands } from "../themes/Provider";

const Services = ({ children }: { children: React.ReactNode }) => {
  return (
    <InitSubscriptions>
      <PickerService>
        <CommandService>
          <ThemeCommands>
            <NotificationService>
              <DisplayService>{children}</DisplayService>
            </NotificationService>
          </ThemeCommands>
        </CommandService>
      </PickerService>
    </InitSubscriptions>
  );
};

export default React.memo(Services);
