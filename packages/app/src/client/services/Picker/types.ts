import { ReactNode } from "react";

export type PickerOption<Data = any> = {
  id: string;
  text: string;
  data?: Data;
};

export type PickerListProps = {
  selectedIndex: number;
  options: PickerOption[];
  onSelect: (selectedOption: PickerOption) => void;
  secondaryAction?: (option: PickerOption) => ReactNode;
};

export type PickerProvider = {
  title?: string;
  // Disable's the filtering of options
  disableFilter?: boolean;
  // Disable input
  disableInput?: boolean;
  // Validate input as user is typing, return "true" if valid or an error message to display to the user
  textValidator?: (text: string) => true | string;
  activationPrefix: string;
  onSelected: (option: PickerOption, inputText?: string) => void;
  options: PickerOption[];
  secondaryAction?: (option: PickerOption) => ReactNode;
  dataTest?: string;
};

export type PickerApi = {
  register: (provider: PickerProvider) => void;
  unregister: (provider: PickerProvider) => void;
  setText: (text: string) => void;
};

export type PickerState = {
  activeProviderIndex: number;
  text: null | string;
  providers: PickerProvider[];
};
