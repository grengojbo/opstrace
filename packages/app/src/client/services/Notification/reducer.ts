import { createReducer, ActionType, createAction } from "typesafe-actions";
import { Notification, NotificationServiceState } from "./types";

export const actions = {
  register: createAction("REGISTER_NOTIFICATION")<Notification>(),
  unregister: createAction("UNREGISTER_NOTIFICATION")<Notification>(),
  unregisterAll: createAction("UNREGISTER_ALL")(),
  changeVisibility: createAction("CHANGE_VISIBILITY")()
};

type Actions = ActionType<typeof actions>;

function removeNotification(
  notifications: Notification[],
  notification: Notification
): Notification[] {
  return notifications.filter(n => n.id !== notification.id);
}

function findNotification(
  notifications: Notification[],
  notification: Notification
): Notification | undefined {
  return notifications.find(n => n.id === notification.id);
}

export const initialState: NotificationServiceState = {
  notifications: [],
  visibility: false
};

export const notificationServiceReducer = createReducer<
  NotificationServiceState,
  Actions
>(initialState)
  .handleAction(actions.register, (state, action): NotificationServiceState => {
    if (findNotification(state.notifications, action.payload)) {
      return state;
    }

    const notifications = [action.payload].concat(state.notifications);
    return {
      ...state,
      visibility: true,
      notifications
    };
  })
  .handleAction(
    actions.unregister,
    (state, action): NotificationServiceState => {
      const notifications = removeNotification(
        state.notifications,
        action.payload
      );

      return {
        ...state,
        notifications
      };
    }
  )
  .handleAction(
    actions.unregisterAll,
    (state, _): NotificationServiceState => ({
      ...state,
      notifications: []
    })
  )
  .handleAction(
    actions.changeVisibility,
    (state, _): NotificationServiceState => ({
      ...state,
      visibility: !state.visibility
    })
  );
