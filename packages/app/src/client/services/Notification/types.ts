export type NotificationState = "error" | "warning" | "info" | "success";

export type Notification = {
  id: string;
  title: string;
  information: string;
  state?: NotificationState;
  actions?: NotificationsActions[];
  handleClose?: () => void;
};

export type NotificationsActions = {
  name: string;
  handler: () => void;
};

export type NotificationServiceState = {
  notifications: Notification[];
  visibility: boolean;
};

export type NotificationServiceApi = {
  register: (notification: Notification) => void;
  unregister: (notification: Notification) => void;
  unregisterAll: () => void;
  changeVisibility: () => void;
};
