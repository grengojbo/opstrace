import * as yup from "yup";

import { InhibitRule } from "./types";
import { labelNameSchema } from "./common";

export const inhibitRuleSchema: yup.SchemaOf<InhibitRule> = yup
  .object({
    target_match: yup
      .object()
      .meta({
        comment:
          "Matchers that have to be fulfilled in the alerts to be muted.",
        example: "<labelname>: <labelvalue>"
      })
      .notRequired(),
    target_match_re: yup
      .object()
      .meta({
        comment:
          "Regex matchers that have to be fulfilled in the alerts to be muted.",
        example: "<labelname>: <regex>"
      })
      .notRequired(),
    source_match: yup
      .object()
      .meta({
        comment:
          "Matchers for which one or more alerts have to exist for the inhibition to take effect.",
        example: "<labelname>: <labelvalue>"
      })
      .notRequired(),
    source_match_re: yup
      .object()
      .meta({
        comment:
          "Regex matchers for which one or more alerts have to exist for the inhibition to take effect.",
        example: "<labelname>: <regex>"
      })
      .notRequired(),
    equal: yup
      .array()
      .of(labelNameSchema)
      .meta({
        comment:
          "Labels that must have an equal value in the source and target alert for the inhibition to take effect."
      })
      .notRequired()
  })
  .meta({
    url: "https://www.prometheus.io/docs/alerting/latest/configuration/#inhibit_rule"
  })
  .noUnknown();
