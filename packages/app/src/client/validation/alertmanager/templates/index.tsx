import * as yup from "yup";

import { AlertmanagerTemplates } from "./types";

import jsonSchema from "./schema.json";

const alertmanagerTemplatesSchema: yup.SchemaOf<AlertmanagerTemplates> = yup.object(
  {
    default_template: yup.string().required()
  }
);

export { alertmanagerTemplatesSchema, jsonSchema };
