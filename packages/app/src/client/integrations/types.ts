import { Integration } from "state/integration/types";
import { Tenant } from "state/tenant/types";

export type NewIntegration<Data> = {
  name: string;
  data: Data;
};

export type NewIntegrationOptions = {
  createGrafanaFolder?: boolean;
};

export type FormProps<Data> = {
  handleCreate: (
    data: NewIntegration<Data>,
    options?: NewIntegrationOptions
  ) => void;
};

export type ShowProps = {};

export type StatusProps = {
  integration: Integration;
  tenant: Tenant;
};

export type IntegrationDef<FormData = {}> = {
  kind: string;
  category: string;
  label: string;
  desc: React.ReactNode;
  Form: React.ComponentType<FormProps<FormData>>;
  Show: React.ComponentType<ShowProps>;
  Status: React.ComponentType<StatusProps>;
  enabled: boolean;
  Logo?: string;
};

export type IntegrationDefs = IntegrationDef[];
export type IntegrationDefRecords = Record<string, IntegrationDef>;
