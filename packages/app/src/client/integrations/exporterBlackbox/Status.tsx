import React from "react";

import { Integration } from "state/integration/types";
import { Tenant } from "state/tenant/types";

import ExporterStatus from "client/integrations/common/ExporterStatus";

export const ExporterBlackboxStatus = (props: {
  integration: Integration;
  tenant: Tenant;
}) => (
  <ExporterStatus
    {...props}
    errorFilter={`|= "level=error"`}
    activeFilter={`!= "level=error"`}
  />
);

export default ExporterBlackboxStatus;
