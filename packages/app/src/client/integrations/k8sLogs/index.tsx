import { K8sLogsForm } from "./Form";
import { K8sLogsShow } from "./Show";
import K8sLogsStatus from "./Status";
import K8sLogsLogo from "./Logo.png";

import { IntegrationDef } from "client/integrations/types";

export const k8sLogsIntegration: IntegrationDef = {
  kind: "k8s-logs",
  category: "infrastructure",
  label: "Kubernetes Logs",
  desc:
    "Generate all the yaml required to send logs from your Kubernetes cluster to this tenant. We'll install bundled dashboards for monitoring Kubernetes with this integration.",
  Form: K8sLogsForm,
  Show: K8sLogsShow,
  Status: K8sLogsStatus,
  enabled: true,
  Logo: K8sLogsLogo
};
