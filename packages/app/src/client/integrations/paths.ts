import { Integration } from "state/integration/types";
import { IntegrationDef } from "client/integrations/types";
import { Tenant } from "state/tenant/types";

const baseIntegrationsPath = (tenant: Tenant) =>
  `/tenant/${tenant.name}/integrations`;

export const allIntegrationsPath = ({ tenant }: { tenant: Tenant }) =>
  `${baseIntegrationsPath(tenant)}/all`;

export const installedIntegrationsPath = ({ tenant }: { tenant: Tenant }) =>
  `${baseIntegrationsPath(tenant)}/installed`;

export const addIntegrationPath = ({
  tenant,
  integrationDef
}: {
  tenant: Tenant;
  integrationDef: IntegrationDef;
}) => `${baseIntegrationsPath(tenant)}/all/install/${integrationDef.kind}`;

export const showIntegrationPath = ({
  tenant,
  integration
}: {
  tenant: Tenant;
  integration: Integration;
}) => `${baseIntegrationsPath(tenant)}/installed/${integration.id}`;

export const editIntegrationPath = ({
  tenant,
  integration
}: {
  tenant: Tenant;
  integration: Integration;
}) => `${baseIntegrationsPath(tenant)}/installed/${integration.id}/edit`;
