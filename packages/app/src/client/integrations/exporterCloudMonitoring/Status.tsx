import React from "react";

import ExporterStatus from "client/integrations/common/ExporterStatus";
import { StatusProps } from "../types";

export const ExporterCloudMonitoringStatus = (props: StatusProps) => (
  <ExporterStatus
    {...props}
    errorFilter={`|= "level=error"`}
    activeFilter={`|= "Listening on"`}
  />
);

export default ExporterCloudMonitoringStatus;
