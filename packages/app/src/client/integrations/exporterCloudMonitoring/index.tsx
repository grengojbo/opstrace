import Form from "./Form";
import Show from "./Show";
import Status from "./Status";
import Logo from "./Logo.jpg";

import { IntegrationDef } from "client/integrations/types";

export const exporterCloudMonitoringIntegration: IntegrationDef = {
  kind: "exporter-cloud-monitoring",
  category: "exporter",
  label: "Google Cloud Monitoring",
  desc:
    "Pipe any of your metrics from Google Cloud Monitoring into Opstrace. You can select metrics from any of the Google Cloud Services such as BigTable or Load Balancers, as long as you've enabled monitoring on the service in the Google Cloud Console.",
  Form: Form,
  Show: Show,
  Status: Status,
  Logo: Logo,
  enabled: true
};
