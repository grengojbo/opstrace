// Returns the command for a user to locally deploy either prometheusYaml() or promtailYaml() content.
// This assumes the user has downloaded the file locally to a file named 'yamlFilename'.
export function deployYaml(yamlFilename: string, tenantName: string): string {
  return `sed "s/__AUTH_TOKEN__/$(cat tenant-api-token-${tenantName})/g" ${yamlFilename} | kubectl apply -f -`;
}

// Returns the command for a user to locally delete either prometheusYaml() or promtailYaml() content.
// This assumes the user has downloaded the file locally to a file named 'yamlFilename'.
// This includes deleting the namespace, so you should warn the user in case they have other things there.
export function deleteYaml(yamlFilename: string): string {
  return `kubectl delete -f ${yamlFilename}`;
}
