import { K8sMetricsForm } from "./Form";
import { K8sMetricsShow } from "./Show";
import K8sMetricsStatus from "./Status";
import K8sMetricsLogo from "./Logo.png";

import { IntegrationDef } from "client/integrations/types";

export const k8sMetricsIntegration: IntegrationDef = {
  kind: "k8s-metrics",
  category: "infrastructure",
  label: "Kubernetes Metrics",
  desc:
    "Generate all the yaml required to send metrics from your Kubernetes cluster to this tenant. We'll install bundled dashboards for monitoring Kubernetes with this integration.",
  Form: K8sMetricsForm,
  Show: K8sMetricsShow,
  Status: K8sMetricsStatus,
  enabled: true,
  Logo: K8sMetricsLogo
};
