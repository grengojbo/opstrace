import { Button } from "client/components/Button";
import React, { ReactNode } from "react";
import { useSimpleNotification } from "client/services/Notification";
import { saveAs } from "file-saver";

type Props = {
  children: ReactNode;
  config: string;
  filename: string;
};

export const downloadConfigYaml = (filename: string, content: string) => {
  var configBlob = new Blob([content], {
    type: "application/x-yaml;charset=utf-8"
  });
  saveAs(configBlob, filename);
};

const DownloadConfigButton = (props: Props) => {
  const { registerNotification } = useSimpleNotification();
  const handleClick = () => {
    try {
      downloadConfigYaml(props.filename, props.config);
    } catch (error: any) {
      registerNotification({
        state: "error" as const,
        title: "Could not download YAML",
        information: error.message
      });
    }
  };
  return (
    <Button
      style={{ marginRight: 20 }}
      variant="contained"
      size="small"
      state="primary"
      onClick={handleClick}
    >
      {props.children}
    </Button>
  );
};

export default DownloadConfigButton;
