import React, { useMemo } from "react";

import useWindowSize from "client/hooks/useWindowSize";

import { YamlEditor } from "client/components/Editor";

import { Box } from "client/components/Box";

export const ViewConfig = ({
  filename,
  config,
  maxWidth = 1000,
  fixedHeight,
  heightBuffer = 0
}: {
  filename: string;
  config: string;
  maxWidth?: number;
  fixedHeight?: number;
  heightBuffer?: number;
}) => {
  const size = useWindowSize();

  const [boxWidth, boxHeight] = useMemo(() => {
    const margins = 32 * 2 + 25 * 2 + 30;
    const width = (size.width > maxWidth ? maxWidth : size.width) - margins;
    const height =
      (fixedHeight ? fixedHeight : size.height) - margins - heightBuffer;

    return [width, height];
  }, [size.width, size.height, maxWidth, fixedHeight, heightBuffer]);

  return (
    <Box width={`${boxWidth}px`} height={`${boxHeight}px`}>
      <YamlEditor filename={filename} data={config} configViewer={true} />
    </Box>
  );
};
