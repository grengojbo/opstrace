import React from "react";
import styled from "styled-components";

import MuiCardHeader, { CardHeaderProps } from "@material-ui/core/CardHeader";

const BaseCardHeader = (props: CardHeaderProps) => <MuiCardHeader {...props} />;

const CardHeader = styled(BaseCardHeader)`
  padding: 0px;
`;

export default CardHeader;
