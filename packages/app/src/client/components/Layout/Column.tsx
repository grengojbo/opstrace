import React from "react";
import Box from "@material-ui/core/Box";
import * as constants from "./constants";
import Panel from "./Panel";

export type ColumnProps = {
  minHeight?: number;
  children: React.ReactNode;
};

const Column = (props: ColumnProps) => {
  let children = props.children;

  return (
    <Box
      flexGrow={1}
      display="flex"
      flexDirection="column"
      minHeight={props.minHeight || constants.MIN_ITEM_HEIGHT}
      minWidth={constants.MIN_ITEM_WIDTH}
      className="LayoutColumn"
    >
      <Panel minHeight={props.minHeight}>{children}</Panel>
    </Box>
  );
};

Column.displayName = "LayoutColumn";

export default Column;
