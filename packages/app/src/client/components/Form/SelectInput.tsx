import React from "react";
import { Controller, Control, FieldPath } from "react-hook-form";
import { HelpCircle } from "react-feather";

import {
  FormControlLabel,
  FormLabel,
  FormHelperText,
  Radio,
  RadioGroup
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { Typography } from "client/components/Typography";
import { Box } from "client/components/Box";

type SelectInputOptionProps = {
  label: React.ReactNode;
  value: string;
};

type SelectInputProps<TFieldValues> = {
  name: FieldPath<TFieldValues>;
  label?: React.ReactNode;
  helperText?: string | React.ReactNode | (() => React.ReactNode);
  optionsProps?: SelectInputOptionProps[];
  control: Control<TFieldValues>;
  labelClass?: string;
  controlClass?: string;
};

const useStyles = makeStyles(theme => ({
  helperText: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap"
  },
  helperIcon: {
    marginRight: 5
  }
}));

export const SelectInput = <TFieldValues,>({
  name,
  label,
  optionsProps = [],
  helperText,
  control,
  labelClass,
  controlClass
}: SelectInputProps<TFieldValues>) => {
  const classes = useStyles();

  return (
    <Controller
      render={({ field }) => (
        <Box>
          {label && (
            <div className={labelClass}>
              <FormLabel>
                <Typography variant="h6" color="textSecondary">
                  {label}
                </Typography>
              </FormLabel>
            </div>
          )}
          <div className={controlClass}>
            <RadioGroup {...field}>
              {optionsProps.map(optionProps => (
                <FormControlLabel control={<Radio />} {...optionProps} />
              ))}
            </RadioGroup>
            {helperText && (
              <FormHelperText>
                <Typography variant="caption" className={classes.helperText}>
                  <HelpCircle
                    width={12}
                    height={12}
                    className={classes.helperIcon}
                  />
                  <span>{helperText}</span>
                </Typography>
              </FormHelperText>
            )}
          </div>
        </Box>
      )}
      control={control}
      name={name}
    />
  );
};
