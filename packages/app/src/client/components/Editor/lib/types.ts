export type EditorOptions = NonNullable<
  Parameters<typeof monaco.editor.create>[1]
>;

export type YamlEditorProps = {
  filename: string;
  jsonSchema?: object;
  data: string;
  onChange?: (data: string, fileName: string) => void;
  configViewer?: boolean;
};
