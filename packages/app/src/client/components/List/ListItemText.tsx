import React from "react";

import MuiListItemText, {
  ListItemTextProps
} from "@material-ui/core/ListItemText";

export const ListItemText = (props: ListItemTextProps) => (
  <MuiListItemText {...props} />
);
