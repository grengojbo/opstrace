export interface Form<DataType extends {} = {}> {
  type: string;
  code: string;
  status: string;
  data: DataType;
}

export type FormRecords<DataType extends {} = {}> = Record<
  string,
  Form<DataType>
>;
