import { split } from "ramda";

import getSubscriptionID from "state/utils/getSubscriptionID";

import { Form } from "./types";

const SEPERATOR = "/";

export const generateFormId = (type: string, code?: string) =>
  `${type}${SEPERATOR}${code || getSubscriptionID()}`;

export const makeFormId = (form: Form) => `${form.type}/${form.code}`;

export const expandFormId = (id: string): { type: string; code: string } => {
  const [type, code] = split(SEPERATOR)(id);
  return { type, code };
};

export const newForm = <DataType extends {} = {}>(
  type: string,
  code: string,
  status: string = "active",
  data: DataType
) => ({
  type,
  code,
  status,
  data
});
