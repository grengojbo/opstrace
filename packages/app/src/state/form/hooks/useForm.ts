import { useEffect, useMemo } from "react";
import { path } from "ramda";
import { useDispatch, useSelector, State } from "state/provider";
import { createSelector } from "reselect";

import { registerForm, unregisterForm } from "state/form/actions";
import { generateFormId, expandFormId, newForm } from "state/form/utils";
import { Form } from "state/form/types";

type formProps<DataType extends {} = {}> = {
  type: string;
  code?: string;
  status?: string;
  data?: DataType;
  unregisterOnUnmount?: boolean;
};

const useForm = <DataType extends {} = {}>({
  type,
  code,
  status,
  data,
  unregisterOnUnmount
}: formProps<DataType>) => {
  const dispatch = useDispatch();
  const id = useMemo(() => generateFormId(type, code), [type, code]);

  useEffect(() => {
    dispatch(registerForm({ id, status, data }));
    return () => {
      if (unregisterOnUnmount === true) dispatch(unregisterForm(id));
    };
  }, [dispatch, id, status, data, unregisterOnUnmount]);

  return id;
};

const selectForm = <DataType extends {}>(
  {
    type,
    code
  }: {
    type: string;
    code: string;
  },
  defaultData?: DataType
) =>
  createSelector(
    () => type,
    () => code,
    (state: State) => state.form,
    (type, code, formState) => {
      let form = path<Form<DataType>>([type, code])(formState);

      if (!form && defaultData)
        form = newForm<DataType>(type, code, "unknown", defaultData);

      return form;
    }
  );

const useFormState = <DataType extends {} = {}>(
  id: string,
  defaultData?: DataType
) => useSelector(selectForm<DataType>(expandFormId(id), defaultData));

export default useForm;
export { useForm, useFormState };
