export type OpstraceConfig = {
  buildInfo?: OpstraceBuildInfo;
};

export type OpstraceBuildInfo = {
  branch: string;
  version: string;
  commit: string;
  buildTime: string;
  buildHostname: string;
};
