import { createAction } from "typesafe-actions";

import { OpstraceBuildInfo } from "./types";

export const updateOpstraceBuildInfo = createAction(
  "UPDATE_OPSTRACE_BUILD_INFO"
)<{
  buildInfo: OpstraceBuildInfo;
}>();
