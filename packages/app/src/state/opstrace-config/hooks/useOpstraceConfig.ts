import { useSelector, State } from "state/provider";

export const getOpstraceConfig = (state: State) => state.opstrace;

export default function useOpstraceConfig() {
  const opstraceConfig = useSelector(getOpstraceConfig);

  // todo: dispatch a call to load the config if it's not here.
  // it's static so don't need a subscription
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   const subId = getSubscriptionID();
  //   dispatch(subscribeToCortexConfig(subId));
  //   return () => {
  //     dispatch(unsubscribeFromCortexConfig(subId));
  //   };
  // }, [dispatch]);

  return opstraceConfig;
}
