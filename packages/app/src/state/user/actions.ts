import { createAction } from "typesafe-actions";
import { SubscriptionID, Users } from "./types";

export const setCurrentUser = createAction("SET_CURRENT_USER")<string>();

export const requestSetDarkMode = createAction(
  "REQUEST_SET_DARK_MODE"
)<boolean>();
export const setDarkMode = createAction("SET_DARK_MODE")<boolean>();

export const subscribeToUserList = createAction(
  "SUBSCRIBE_USER_LIST"
)<SubscriptionID>();

export const unsubscribeFromUserList = createAction(
  "UNSUBSCRIBE_USER_LIST"
)<SubscriptionID>();
export const setUserList = createAction("SET_USER_LIST")<Users>();
export const deleteUser = createAction("DELETE_USER")<string>();
export const addUser = createAction("ADD_USER")<string>();
