import { useEffect } from "react";
import { values } from "ramda";
import { createSelector } from "reselect";
import { useDispatch, useSelector, State } from "state/provider";

import {
  subscribeToTenantList,
  unsubscribeFromTenantList
} from "state/tenant/actions";
import getSubscriptionID from "state/utils/getSubscriptionID";

export const selectTenantList = createSelector(
  (state: State) => state.tenants.loading,
  (state: State) => state.tenants.tenants,
  (loading, tenants) => (loading ? [] : values(tenants))
);

export default function useTenantList() {
  useTenantListSubscription();
  return useSelector(selectTenantList);
}

export const useTenantListSubscription = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const subId = getSubscriptionID();
    dispatch(subscribeToTenantList(subId));
    return () => {
      dispatch(unsubscribeFromTenantList(subId));
    };
  }, [dispatch]);
};
