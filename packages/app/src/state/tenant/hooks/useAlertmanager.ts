import { createSelector } from "reselect";
import { useSelector, useDispatch, State } from "state/provider";

import { selectTenant } from "./useTenant";
import { getAlertmanager } from "state/tenant/actions";

export const selectAlertmanager = createSelector(
  selectTenant,
  tenant => tenant?.alertmanager
);

export default function useAlertmanager(tenantName: string) {
  const data = useSelector((state: State) =>
    selectAlertmanager(state, tenantName)
  );

  const dispatch = useDispatch();

  if (!data) dispatch(getAlertmanager(tenantName));

  return data;
}
