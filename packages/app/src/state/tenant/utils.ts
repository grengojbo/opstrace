import { none, propEq } from "ramda";

import { Tenant } from "./types";

export const isTenantNameUnique = (tenantName: string, tenants: Tenant[]) =>
  none(propEq("name", tenantName))(tenants);
