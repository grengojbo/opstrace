import { SubscribeToTenantListSubscription } from "state/clients/graphqlClient";

export type Alertmanager = {
  config: string;
  online?: boolean;
};

type TenantVirtualFields = {
  alertmanager?: Alertmanager;
};

export type Tenant = SubscribeToTenantListSubscription["tenant"][0] &
  TenantVirtualFields;

export type Tenants = Tenant[];
export type TenantRecords = Record<string, Tenant>;

// use this same id to unsubscribe
export type SubscriptionID = number;
