import { all, call, spawn, takeEvery, put } from "redux-saga/effects";
import axios from "axios";
import yaml from "js-yaml";
import { ServerError } from "server/errors";

import * as actions from "../actions";

import cortexConfigSubscriptionManager from "./cortexConfigSubscription";
import { GrafanaError } from "client/utils/grafana";

export default function* userTaskManager() {
  const sagas = [cortexConfigSubscriptionManager, saveRuntimeConfigListener];
  // technique to keep the root alive and spawn sagas into their
  // own retry-on-failure loop.
  // https://redux-saga.js.org/docs/advanced/RootSaga.html
  yield all(
    sagas.map(saga =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e: any) {
            console.error(e);
          }
        }
      })
    )
  );
}

function* saveRuntimeConfigListener() {
  yield takeEvery(actions.saveCortexRuntimeConfig, saveRuntimeConfig);
}

function* saveRuntimeConfig(
  action: ReturnType<typeof actions.saveCortexRuntimeConfig>
) {
  try {
    yield axios.request({
      method: "POST",
      url: "/_/cortex/runtime_config",
      headers: {
        "Content-Type": "text/plain"
      },
      data: yaml.dump(action.payload)
    });
  } catch (err: any) {
    if (
      axios.isAxiosError(err) &&
      err.response &&
      ServerError.isInstance(err.response.data)
    ) {
      // Extract the specific error message
      yield put(
        actions.saveCortexRuntimeConfigError(
          (err as GrafanaError).response.data.message
        )
      );
    } else {
      yield put(actions.saveCortexRuntimeConfigError(err.toString()));
    }
  }
}
