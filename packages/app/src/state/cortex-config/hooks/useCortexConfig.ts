import { useEffect } from "react";
import { createSelector } from "reselect";
import { useDispatch, useSelector, State } from "state/provider";
import {
  subscribeToCortexConfig,
  unsubscribeFromCortexConfig
} from "../actions";
import getSubscriptionID from "state/utils/getSubscriptionID";

export const getCortexConfig = (state: State) => state.cortex;

export const getCortexConfigLoaded = createSelector(
  getCortexConfig,
  cortex =>
    !cortex.loadingConfig &&
    !cortex.loadingRecognizedRuntimeConfig &&
    !cortex.loadingRuntimeConfig
);

export function useCortexConfigLoaded() {
  return useSelector(getCortexConfigLoaded);
}

/**
 * Subscribes to cortex config and will update on
 * any changes. Automatically unsubscribeFromCortexConfigs
 * on unmount.
 */
export default function useCortexConfig() {
  const cortexConfig = useSelector(getCortexConfig);
  const dispatch = useDispatch();

  useEffect(() => {
    const subId = getSubscriptionID();
    dispatch(subscribeToCortexConfig(subId));
    return () => {
      dispatch(unsubscribeFromCortexConfig(subId));
    };
  }, [dispatch]);

  return cortexConfig;
}
