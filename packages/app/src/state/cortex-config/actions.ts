import { createAction } from "typesafe-actions";
import { SubscriptionID, Config, RuntimeConfig } from "./types";

export const setCortexRuntimeConfig = createAction(
  "SET_CORTEX_RUNTIME_CONFIG"
)<RuntimeConfig>();
export const updateCortexRuntimeConfig = createAction(
  "UPDATE_CORTEX_RUNTIME_CONFIG_OPTION"
)<{
  tenant: string;
  configOption: string;
  value: string | boolean | number | undefined;
}>();
export const deleteCortexRuntimeConfig = createAction(
  "DELETE_CORTEX_RUNTIME_CONFIG_OPTION"
)<{
  tenant: string;
  configOption: string;
}>();
export const setRecognizedCortexRuntimeConfig = createAction(
  "SET_CORTEX_RECOGNIZED_RUNTIME_CONFIG"
)<RuntimeConfig>();
export const setCortexConfig = createAction("SET_CORTEX_CONFIG")<Config>();

export const setCortexConfigError = createAction(
  "SET_CORTEX_CONFIG_LOADING_ERROR"
)<string>();

export const saveCortexRuntimeConfig = createAction(
  "SAVE_CORTEX_RUNTIME_CONFIG"
)<RuntimeConfig>();
export const saveCortexRuntimeConfigError = createAction(
  "SAVE_CORTEX_RUNTIME_CONFIG_ERROR"
)<string>();

export const subscribeToCortexConfig = createAction(
  "SUBSCRIBE_CORTEX_CONFIG"
)<SubscriptionID>();

export const unsubscribeFromCortexConfig = createAction(
  "UNSUBSCRIBE_CORTEX_CONFIG"
)<SubscriptionID>();
