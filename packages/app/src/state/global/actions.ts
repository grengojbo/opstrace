import { createAction } from "typesafe-actions";

import { Tenant } from "state/tenant/types";

export const selectedTenantChanged = createAction(
  "global/SELECTED_TENANT_CHANGED"
)<{ tenant: Tenant }>();
