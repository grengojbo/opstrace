import { combineReducers } from "redux";
import { reducer as userReducer } from "./user/reducer";
import { reducer as tenantReducer } from "./tenant/reducer";
import { reducer as integrationReducer } from "./integration/reducer";
import { reducer as cortexConfigReducer } from "./cortex-config/reducer";
import { reducer as opstraceConfigReducer } from "./opstrace-config/reducer";
import { reducer as formReducer } from "./form/reducer";
import { notificationServiceReducer } from "client/services/Notification/reducer";

export const mainReducers = {
  users: userReducer,
  tenants: tenantReducer,
  integrations: integrationReducer,
  form: formReducer,
  cortex: cortexConfigReducer,
  opstrace: opstraceConfigReducer,
  notifications: notificationServiceReducer
};

export const mainReducer = combineReducers(mainReducers);
export type State = ReturnType<typeof mainReducer>;
