import getSubscriptionID from "../getSubscriptionID";

beforeEach(() => {
  jest.spyOn(global.Math, "random").mockReturnValue(0.12);
});

afterEach(() => {
  jest.spyOn(global.Math, "random").mockRestore();
});

test("getSubscriptionID returns correct value", () => {
  expect(getSubscriptionID()).toEqual(12000000);
});
