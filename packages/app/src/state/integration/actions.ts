import { createAction } from "typesafe-actions";

import {
  Integration,
  Integrations,
  IntegrationGrafana,
  SubscriptionID
} from "./types";

export const addIntegration = createAction("ADD_INTEGRATION")<{
  integration: Integration;
}>();
export const deleteIntegration = createAction("DELETE_INTEGRATION")<{
  tenantId: string;
  id: string;
}>();
export const subscribeToIntegrationList = createAction(
  "SUBSCRIBE_INTEGRATION_LIST"
)<{ subId: SubscriptionID; tenant: string }>();

export const unsubscribeFromIntegrationList = createAction(
  "UNSUBSCRIBE_INTEGRATION_LIST"
)<{ subId: SubscriptionID; tenant: string }>();

export const updateIntegrations = createAction(
  "UPDATE_INTEGRATIONS"
)<Integrations>();

export const clearIntegrations = createAction("CLEAR_INTEGRATIONS")();

export const loadGrafanaStateForIntegration = createAction(
  "LOAD_GRAFANA_STATE_FOR_INTEGRATION"
)<{ id: string }>();

export const updateGrafanaStateForIntegration = createAction(
  "UPDATE_GRAFANA_STATE_FOR_INTEGRATION"
)<{ id: string; grafana: IntegrationGrafana }>();
