import { useParams } from "react-router";
import { createSelector } from "reselect";

import { useSelector, State } from "state/provider";

import { Integration } from "state/integration/types";
import { useIntegrationListSubscription } from "./useIntegrationList";

export const selectIntegration = createSelector(
  (state: State) => state.integrations.loading,
  (state, _) => state.integrations.integrations,
  (_: State, id: string) => id,
  (loading, integrations, id: string) =>
    loading === false ? integrations[id] : null
);

export function useSelectedIntegration() {
  const params = useParams<{ integrationId: string }>();
  return useIntegration(params.integrationId || "");
}

export default function useIntegration(
  id: string
): Integration | null | undefined {
  useIntegrationListSubscription();
  // can return undefined if the id does not exist
  return useSelector((state: State) => selectIntegration(state, id));
}
