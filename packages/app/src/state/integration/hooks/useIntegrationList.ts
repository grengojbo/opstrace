import { useEffect } from "react";
import { values } from "ramda";
import { createSelector } from "reselect";
import { useDispatch, useSelector, State } from "state/provider";

import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";

import {
  subscribeToIntegrationList,
  unsubscribeFromIntegrationList
} from "../actions";
import getSubscriptionID from "state/utils/getSubscriptionID";

export const selectIntegrationList = createSelector(
  (state: State) => state.integrations.loading,
  (state: State) => state.integrations.integrations,
  (loading, integrations) => (loading ? [] : values(integrations))
);

export const selectIntegrationListCount = createSelector(
  selectIntegrationList,
  integrationList => integrationList.length
);

export const useIntegrationList = () => {
  useIntegrationListSubscription();
  return useSelector(selectIntegrationList);
};

export const useIntegrationListCount = () => {
  useIntegrationListSubscription();
  return useSelector(selectIntegrationListCount);
};

export const useIntegrationListSubscription = () => {
  const tenant = useSelectedTenantWithFallback();
  const dispatch = useDispatch();

  useEffect(() => {
    // if (tenant) {
    const subId = getSubscriptionID();
    // make local ref of this inside useEffect scope so unsubscribe works
    const tenantName = tenant.name;
    dispatch(subscribeToIntegrationList({ subId, tenant: tenantName }));

    return () => {
      dispatch(unsubscribeFromIntegrationList({ subId, tenant: tenantName }));
    };
    // } else return null;
  }, [dispatch, tenant.name]);
};
