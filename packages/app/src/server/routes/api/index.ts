import express from "express";

import { GeneralServerError } from "server/errors";
import authRequired from "server/middleware/auth";

import createAuthHandler from "./authentication";
import { pubUiCfgHandler, buildInfoHandler } from "./uicfg";
import createGraphqlHandler from "./graphql";
import createCortexHandler from "./cortex";

function createAPIRoutes(): express.Router {
  const api = express.Router();
  api.use("/auth", createAuthHandler());
  api.use("/public-ui-config", pubUiCfgHandler);
  api.use("/buildinfo", buildInfoHandler);

  // Authentication required
  api.use("/graphql", authRequired, createGraphqlHandler());
  api.use("/cortex", authRequired, createCortexHandler());

  api.all("*", function (req, res, next) {
    next(new GeneralServerError(404, "api route not found"));
  });

  return api;
}

export default createAPIRoutes;
