import http from "http";
import url from "url";
import express from "express";
import sessionParser from "server/middleware/session";
import { graphqlProxy, getHasuraSessionHeaders } from "./api/graphql";
import createWebsocketServer from "./api/sockets";
import { log } from "@opstrace/utils/lib/log";

export default function setupWebsocketHandling(server: http.Server) {
  createWebsocketServer(server);

  // Listen for upgrade requests and handle appropriately
  server.on("upgrade", function upgrade(req, socket, head) {
    const pathname = url.parse(req.url).pathname;
    // https://github.com/websockets/ws/tree/3d5066a7cad9fe3176002916aeda720a7b5ee419#multiple-servers-sharing-a-single-https-server
    if (pathname === "/_/graphql") {
      // https://github.com/websockets/ws/blob/master/examples/express-session-parse/index.js
      sessionParser(req, {} as express.Response, () => {
        if (!(req.session && req.session.userId)) {
          // This will show in the browser console (using the apolloClient) as "failed: HTTP Authentication failed; no valid credentials available"
          socket.write("HTTP/1.1 401 Unauthorized\r\n\r\n");
          socket.destroy();
          return;
        }

        graphqlProxy.ws(
          req,
          socket,
          head,
          {
            ignorePath: true,
            headers: getHasuraSessionHeaders(req.session.userId)
          },
          (err: Error) => {
            log.warning(
              "error in graphql websocket proxy upstream (ignoring): %s",
              err
            );
          }
        );
      });
    } else if (pathname === "/_/socket/") {
      sessionParser(req, {} as express.Response, () => {
        if (!(req.session && req.session.userId)) {
          // This will show in the browser console (using the apolloClient) as "failed: HTTP Authentication failed; no valid credentials available"
          socket.write("HTTP/1.1 401 Unauthorized\r\n\r\n");
          socket.destroy();
          return;
        }
      });
    } else {
      log.info(`socket upgrade request to unknown endpoint ${pathname}`);
      socket.destroy();
    }
  });
}
