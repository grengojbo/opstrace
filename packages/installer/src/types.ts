export type EnsureInfraExistsResponse = {
  kubeconfigString: string;
  postgreSQLEndpoint: string;
  opstraceDBName: string;
  // Only applicable to AWS
  certManagerRoleArn?: string;
};
