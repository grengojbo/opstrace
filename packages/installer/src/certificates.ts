import {
  V1CertificaterequestResourceType,
  V1CertificateResourceType
} from "@opstrace/kubernetes";
import { log } from "@opstrace/utils";

//
// Note: workaround for https://github.com/opstrace/opstrace/issue/151 until
// https://github.com/jetstack/cert-manager/issues/3594 is resolved.
//
// If the https-cert certificate is in the failed state then delete it. The
// controller will recreate the resource thereby triggering a restart of the
// certificate request process.
//
// We also delete the CertificateRequest otherwise cert-manager will see that
// the certificate already has a request in flight and will not recreate it.
//
export function handleFailedCertificate(
  certificates: V1CertificateResourceType[],
  certificateRequests: V1CertificaterequestResourceType[]
) {
  log.debug("checking if certificate is in failed state");

  const cert = certificates.find(cert => {
    return cert.spec.status?.conditions?.find(cond => {
      log.debug(`certificate condition: ${cond.message}`);
      return cond.message!.includes("Failed to wait for order resource");
    });
  });

  if (cert === undefined) {
    log.debug("no certificate in failed state");
    return;
  }

  log.debug(
    `${cert.name} possibly in a failed state, deleting to retrigger request`
  );

  const certRequest = certificateRequests.find(cert => {
    return cert.name.startsWith(cert.name);
  });

  certRequest
    ?.delete()
    .catch(e =>
      log.debug(`error deleting certificate request ${certRequest.name}: ${e}`)
    )
    .finally(() => {
      log.debug(
        `deleted certificate request ${certRequest.name} in failed state: `
      );
    });

  cert
    ?.delete()
    .catch(e => log.debug(`error deleting certificate ${cert.name}: ${e}`))
    .finally(() => {
      log.debug("deleted certificate ${cert.name} in failed state");
    });
}
