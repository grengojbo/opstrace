import {
  ResourceCollection,
  Service,
  V1ServicemonitorResource
} from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";

export function ControllerServiceMonitorResources(
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  // Have the controller create a Service that points back to itself.
  // This is required for the controller ServiceMonitor below to work.
  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          labels: {
            name: "opstrace-controller",
            tenant: "system"
          },
          name: "opstrace-controller",
          namespace: "kube-system"
        },
        spec: {
          ports: [
            {
              name: "metrics",
              port: 8900,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "metrics" as any
            }
          ],
          selector: {
            name: "opstrace-controller"
          }
        }
      },
      kubeConfig
    )
  );

  // ServiceMonitor in system-tenant that scrapes the Service in kube-system.
  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            name: "opstrace-controller",
            tenant: "system"
          },
          name: "opstrace-controller",
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              port: "metrics"
            }
          ],
          jobLabel: "name",
          namespaceSelector: {
            matchNames: ["kube-system"]
          },
          selector: {
            matchLabels: {
              name: "opstrace-controller"
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}
