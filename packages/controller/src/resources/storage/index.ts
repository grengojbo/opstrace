import { ResourceCollection } from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { getControllerConfig } from "../../helpers";
import { KubeConfig } from "@kubernetes/client-node";
import { StorageResources as GCPStorageResources } from "./gcp";
import { StorageResources as AWSStorageResources } from "./aws";

export function StorageResources(
  state: State,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();
  const { target } = getControllerConfig(state);
  if (target === "gcp") {
    collection.add(GCPStorageResources(state, kubeConfig));
  }
  if (target === "aws") {
    collection.add(AWSStorageResources(state, kubeConfig));
  }

  return collection;
}
