import { ResourceCollection, StorageClass } from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { KubeConfig } from "@kubernetes/client-node";

export function StorageResources(
  state: State,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  collection.add(
    new StorageClass(
      {
        apiVersion: "storage.k8s.io/v1",
        kind: "StorageClass",
        metadata: {
          name: "pd-ssd"
        },
        parameters: {
          type: "gp2"
        },
        provisioner: "kubernetes.io/aws-ebs",
        volumeBindingMode: "WaitForFirstConsumer",
        allowVolumeExpansion: true
      },
      kubeConfig
    )
  );

  return collection;
}
