import { KubeConfig } from "@kubernetes/client-node";
import {
  Namespace,
  ResourceCollection,
  V1ClickhouseinstallationResource,
  V1ServicemonitorResource
} from "@opstrace/kubernetes";
import { DockerImages } from "@opstrace/controller-config";
import { select } from "@opstrace/utils";
import { getNodeCount } from "../../helpers";
import { State } from "../../reducer";
import { V1ContainerPort, V1ServicePort } from "@kubernetes/client-node";

// Note: Changing this requires also changing the "opstrace_controller" keys below.
export const CLICKHOUSE_USERNAME = "opstrace_controller";
export const CLICKHOUSE_PASSWORD = "opstrace_controller_password";
// assuming one clickhouse cluster accessible with a single service
export const CLICKHOUSE_CLUSTER_SERVICE_NAME = "cluster";

export function ClickHouseResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  collection.add(
    new Namespace(
      {
        apiVersion: "v1",
        kind: "Namespace",
        metadata: {
          name: namespace
        }
      },
      kubeConfig
    )
  );

  // replicas covers both clickhouse replication and keeper consensus
  // keeper uses raft protocol https://raft.github.io/
  // reference: https://clickhouse.com/docs/en/operations/clickhouse-keeper/#implementation-details
  // raft recommends at least 3 servers for a failure tolerance of 1 server
  // assume for now we'll never need more than 5 which will give us a failure tolerance of 2
  const replicas = select(getNodeCount(state), [
    {
      "<=": 4,
      choose: 3
    },
    {
      "<=": Infinity,
      choose: 5
    }
  ]);

  // port for http access
  const httpPort: V1ContainerPort = {
    name: "http",
    containerPort: 8123
  };
  // port for native client access
  const clientPort: V1ContainerPort = {
    name: "client",
    containerPort: 9000
  };
  // cluster interserver communication port
  const interserverPort: V1ContainerPort = {
    name: "interserver",
    containerPort: 9009
  };
  // keeper zookeeper compatible API port
  const keeperPort: V1ContainerPort = {
    name: "keeper",
    containerPort: 2181
  };
  // raft protocol port
  const raftPort: V1ContainerPort = {
    name: "raft",
    containerPort: 9444
  };
  // metrics port
  const metricsPort: V1ContainerPort = {
    name: "metrics",
    containerPort: 9001
  };

  const servicePorts = (ps: V1ContainerPort[]): V1ServicePort[] =>
    ps.map(
      (p): V1ServicePort => ({
        name: p.name,
        port: p.containerPort
      })
    );

  // each raft server needs list of all other servers in the set
  // server ids are integer identifiers
  // assumes replica service generated name with {chi}-{host} macros
  const raftServers = Array.from({ length: replicas }, (_, k) => k).map(
    i => `
      <server>
        <id>${i}</id>
        <hostname>cluster-0-${i}</hostname>
        <port>${raftPort.containerPort}</port>
      </server>`
  );

  // clickhouse keeper config
  // clickhouse operator doesn't support creation of these configs at the moment
  // config makes use of CLICKHOUSE_REPLICA env var to set the server id for the current server
  // reference: https://clickhouse.com/docs/en/operations/clickhouse-keeper/#configuration
  const keeperConfig = `<yandex>
  <keeper_server>
    <tcp_port>${keeperPort.containerPort}</tcp_port>
    <server_id from_env="CLICKHOUSE_REPLICA"/>
    <log_storage_path>/var/log/clickhouse-server/coordination/log</log_storage_path>
    <snapshot_storage_path>/var/lib/clickhouse/coordination/snapshots</snapshot_storage_path>
    <raft_configuration>${raftServers.join("\n")}
    </raft_configuration>
  </keeper_server>
</yandex>`;

  collection.add(
    new V1ClickhouseinstallationResource(
      {
        apiVersion: "clickhouse.altinity.com/v1",
        kind: "ClickHouseInstallation",
        metadata: {
          // cluster installation name is reused in service naming in the service templates below
          name: CLICKHOUSE_CLUSTER_SERVICE_NAME,
          namespace
        },
        // reference: https://github.com/Altinity/clickhouse-operator/blob/master/deploy/dev/clickhouse-operator-install-yaml-template-01-section-crd-01-chi.yaml#L358
        spec: {
          configuration: {
            clusters: [
              {
                // assume one cluster per installation
                name: "cluster",
                layout: {
                  // assume single shard until we are dealing with significant data storage per node
                  shardsCount: 1,
                  replicasCount: replicas
                }
              }
            ],
            // add ClickHouse settings here, slash-delimited
            // reference: https://clickhouse.com/docs/en/operations/server-configuration-parameters/settings/
            settings: {
              // default replica settings to simplify replicated table creation
              // reference: https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/replication/#creating-replicated-tables
              default_replica_path:
                "/clickhouse/{installation}/{cluster}/tables/{shard}/{database}/{table}",
              default_replica_name: "{replica}"
            },
            users: {
              // Can't use `${CLICKHOUSE_USERNAME}/*` in keys:
              "opstrace_controller/password": `${CLICKHOUSE_PASSWORD}`,
              "opstrace_controller/networks/ip": [
                // Note: could restrict to something like "host local, regexp opstrace-controller-.*\.kube-system\.svc\.cluster\.local"
                "0.0.0.0/0"
              ],
              // controller user can create other users and grant access to them
              "opstrace_controller/access_management": "1"
            },
            // reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#specconfigurationzookeeper
            // zookeeper loops back to local keeper
            zookeeper: {
              nodes: [
                {
                  host: "localhost",
                  port: keeperPort.containerPort
                }
              ]
            },
            // reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#specconfigurationfiles
            files: {
              "keeper.xml": keeperConfig
            }
          },

          defaults: {
            templates: {
              // Replace default PUBLIC LoadBalancer service with an internal ClusterIP service.
              // We don't want ClickHouse accessible to the world!!
              serviceTemplate: "service-template",
              // service used for accessing members of cluster
              replicaServiceTemplate: "replica-template",
              // Configure volumes for storage (/var/lib/clickhouse) and logs (/var/log/clickhouse-server)
              dataVolumeClaimTemplate: "data-volume",
              logVolumeClaimTemplate: "logs-volume",
              // Configure image version, and add capabilities to tidy up startup warnings
              podTemplate: "pod-template"
            }
          },

          templates: {
            // reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#spectemplatespodtemplates
            podTemplates: [
              {
                name: "pod-template",
                // generateName is used by StatefulSet and PodSpec naming
                generateName: "{chi}-{host}",
                spec: {
                  securityContext: {
                    // Required for startup when we specify securityContext in container
                    fsGroup: 101 // clickhouse gid
                  },
                  containers: [
                    {
                      name: "clickhouse",
                      // Default is "latest" tag
                      image: DockerImages.clickhouse,
                      // TODO(nickbp): not actually an option... figure out image pull secrets support
                      //imagePullSecrets: getImagePullSecrets(),
                      securityContext: {
                        // Trying to tidy up startup warnings about missing capabilities,
                        // but doesn't seem to work according to container logs...
                        capabilities: {
                          drop: ["ALL"],
                          add: ["IPC_LOCK", "SYS_NICE"]
                        },
                        // Required for startup when we specify securityContext here
                        runAsUser: 101, // clickhouse uid
                        runAsGroup: 101 // clickhouse gid
                      },
                      ports: [
                        httpPort,
                        clientPort,
                        interserverPort,
                        keeperPort,
                        raftPort,
                        metricsPort
                      ],
                      env: [
                        // derive replica number from field, used in keeper config
                        {
                          name: "CLICKHOUSE_REPLICA",
                          valueFrom: {
                            fieldRef: {
                              fieldPath:
                                "metadata.labels['clickhouse.altinity.com/replica']"
                            }
                          }
                        },
                        // init timeout is used in clickhouse image entrypoint script
                        // increase to 60s to give it chance to initialise keeper cluster
                        {
                          name: "CLICKHOUSE_INIT_TIMEOUT",
                          value: "60"
                        }
                      ],
                      // readiness probe checks for raft port rather than /ping http check
                      // ping http check will only succeed once raft is ready
                      readinessProbe: {
                        tcpSocket: {
                          port: raftPort.name
                        },
                        initialDelaySeconds: 10,
                        periodSeconds: 3
                      },
                      // liveness /ping http check is the standard check for clickhouse
                      livenessProbe: {
                        httpGet: {
                          path: "/ping",
                          port: httpPort.name
                        },
                        initialDelaySeconds: 60,
                        periodSeconds: 3,
                        failureThreshold: 10
                      }
                    }
                  ]
                }
              }
            ],

            // reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#spectemplatesservicetemplates
            serviceTemplates: [
              // generic cluster Service at: cluster.clickhouse.svc.cluster.local
              {
                generateName: "{chi}",
                name: "service-template",
                spec: {
                  // Must be provided manually or else deployment fails
                  // Cluster level service just needs http and client ports
                  ports: servicePorts([httpPort, clientPort]),
                  // Default is a PUBLIC LoadBalancer, we don't want that!
                  type: "ClusterIP"
                }
              },
              // replica specific service used for interserver/keeper replication/sharding
              {
                generateName: "{chi}-{host}",
                name: "replica-template",
                spec: {
                  // Must be provided manually or else deployment fails
                  // Cluster level service just needs http and client ports
                  ports: servicePorts([
                    httpPort,
                    clientPort,
                    interserverPort,
                    keeperPort,
                    raftPort,
                    metricsPort
                  ]),
                  // headless StatefulSet service
                  clusterIP: "None"
                }
              }
            ],

            // reference: https://github.com/Altinity/clickhouse-operator/blob/master/docs/custom_resource_explained.md#spectemplatesvolumeclaimtemplates
            volumeClaimTemplates: [
              {
                name: "data-volume",
                spec: {
                  accessModes: ["ReadWriteOnce"],
                  resources: {
                    requests: {
                      storage: "25Gi" // TODO(nickbp): Configurable data volume size
                    }
                  },
                  storageClassName: "pd-ssd"
                }
              },
              {
                name: "logs-volume",
                spec: {
                  accessModes: ["ReadWriteOnce"],
                  resources: {
                    requests: {
                      storage: "1Gi"
                    }
                  },
                  storageClassName: "pd-ssd"
                }
              }
            ]
          }
        }
      },
      kubeConfig
    )
  );

  // monitor replica services directly
  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          name: "clickhouse",
          namespace,
          labels: {
            app: "clickhouse"
          }
        },
        spec: {
          endpoints: [
            {
              interval: "60s",
              port: "metrics"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              "clickhouse.altinity.com/chi": CLICKHOUSE_CLUSTER_SERVICE_NAME,
              "clickhouse.altinity.com/Service": "host"
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}
