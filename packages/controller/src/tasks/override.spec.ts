import { KubeConfig } from "@kubernetes/client-node";
import {
  ResourceCollection,
  V1Alpha1CortexResource
} from "@opstrace/kubernetes";
import { ControllerOverrides } from "../helpers";

import { overrideHelper } from "./override";

// mock only the logger functions, from
// https://jestjs.io/docs/mock-functions#mocking-partials
// required so we can use deepMerge.
jest.mock("@opstrace/utils", () => {
  const originalModule = jest.requireActual("@opstrace/utils");
  return {
    ...originalModule,
    log: {
      debug: jest.fn,
      warning: jest.fn
    }
  };
});

jest.mock("@kubernetes/client-node");

test("should override the cortex-operator resource", () => {
  const state = new ResourceCollection();
  state.add(
    new V1Alpha1CortexResource(
      {
        apiVersion: "cortex.opstrace.io/v1alpha1",
        kind: "Cortex",
        metadata: {
          name: "opstrace-cortex",
          namespace: "test"
        },
        spec: {
          image: "test",
          querier_spec: {
            replicas: 2
          }
        }
      },
      new KubeConfig()
    )
  );

  const overrides: ControllerOverrides = {
    "Deployment__application__redis-master": { spec: { replicas: 2 } },
    "Cortex__test__opstrace-cortex": {
      spec: { querier_spec: { replicas: 1 } }
    }
  };

  const expected = new V1Alpha1CortexResource(
    {
      apiVersion: "cortex.opstrace.io/v1alpha1",
      kind: "Cortex",
      metadata: {
        name: "opstrace-cortex",
        namespace: "test"
      },
      spec: {
        image: "test",
        querier_spec: {
          replicas: 1
        }
      }
    },
    new KubeConfig()
  ).get();

  overrideHelper(overrides, state);

  expect(state.get()).toHaveLength(1);
  const result = state.get()[0].get();
  expect(result).toMatchObject(expected);
});
