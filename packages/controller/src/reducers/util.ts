export interface ResourceCache<T> {
  loaded: boolean;
  error: Error | null;
  resources: T;
}
