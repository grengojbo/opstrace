import express from "express";
import * as promclient from "prom-client";

import { log } from "@opstrace/utils";

export function setupPromExporter(port: number) {
  // Collect process_* and nodejs_* metrics, see list here:
  // https://github.com/siimon/prom-client/blob/4e6aacd/lib/defaultMetrics.js
  promclient.collectDefaultMetrics();

  const httpapp = express();

  httpapp.get(
    "/metrics",
    async function (req: express.Request, res: express.Response) {
      log.debug("handling request to /metrics");
      res.set("Content-Type", promclient.register.contentType);
      const metrics = await promclient.register.metrics();
      res.send(metrics);
    }
  );

  httpapp.listen(port, () =>
    log.info("metrics server listening on port %s", port)
  );
}
