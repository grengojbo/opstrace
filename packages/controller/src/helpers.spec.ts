import { KubeConfig, V1ConfigMap } from "@kubernetes/client-node";
import { ConfigMap } from "@opstrace/kubernetes";
import { ControllerOverrides, getControllerOverrides } from "./helpers";

// mock logger
jest.mock("@opstrace/utils", () => ({
  log: {
    debug: jest.fn,
    warning: jest.fn
  }
}));

jest.mock("@kubernetes/client-node");

test("should parse a simple config map with controller overrides", () => {
  const resource: V1ConfigMap = {
    data: {
      "Deployment__application__redis-master": `
spec:
 replicas: 2
`
    }
  };
  const kubeconfig = new KubeConfig();
  const cm = new ConfigMap(resource, kubeconfig);

  const expected: ControllerOverrides = {
    "Deployment__application__redis-master": { spec: { replicas: 2 } }
  };

  const result = getControllerOverrides(cm);

  expect(result).toMatchObject(expected);
});

test("should parse a more complex config map with controller overrides", () => {
  const resource: V1ConfigMap = {
    data: {
      "Deployment__application__redis-master": `
spec:
  replicas: 2
`,
      "Cortex__cortex__opstrace-cortex": `
spec:
  querier_spec:
    replicas: 1
`
    }
  };
  const kubeconfig = new KubeConfig();
  const cm = new ConfigMap(resource, kubeconfig);

  const expected: ControllerOverrides = {
    "Deployment__application__redis-master": { spec: { replicas: 2 } },
    "Cortex__cortex__opstrace-cortex": {
      spec: { querier_spec: { replicas: 1 } }
    }
  };

  const result = getControllerOverrides(cm);

  expect(result).toMatchObject(expected);
});
