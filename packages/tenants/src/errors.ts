export class TenantDoesNotExistError extends Error {
  constructor(...args: string[] | undefined[]) {
    super(...args);
    Error.captureStackTrace(this, TenantDoesNotExistError);
  }
}
