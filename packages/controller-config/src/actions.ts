import { createAsyncAction, createAction } from "typesafe-actions";
import { LatestControllerConfigType } from "./schema";

export const actions = {
  set: createAsyncAction(
    "SET_CONFIG_REQUEST",
    "SET_CONFIG_SUCCESS",
    "SET_CONFIG_FAILURE"
  )<
    { config: LatestControllerConfigType },
    { config: LatestControllerConfigType },
    { config: LatestControllerConfigType; error: Error }
  >(),

  destroy: createAsyncAction(
    "DESTROY_CONFIG_REQUEST",
    "DESTROY_CONFIG_SUCCESS",
    "DESTROY_CONFIG_FAILURE"
  )<{ name: string }, { name: string }, { name: string; error: Error }>(),

  fetchAll: createAsyncAction(
    "FETCH_CONFIG_REQUEST",
    "FETCH_CONFIG_SUCCESS",
    "FETCH_CONFIG_FAILURE"
  )<undefined, { config: LatestControllerConfigType }, { error: Error }>(),

  subscribe: createAction("SUBSCRIBE_CONFIG")<unknown>(),

  unSubscribe: createAction("UNSUBSCRIBE_CONFIG")<unknown>(),

  onChanged: createAction("ON_CONFIG_CHANGED")<{
    config: LatestControllerConfigType;
  }>()
};
