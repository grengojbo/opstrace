import { ConfigMap, KubeConfiguration } from "@opstrace/kubernetes";
import {
  LatestControllerConfigType,
  LatestControllerConfigSchema
} from "./schema";

export const CONFIGMAP_NAME = "opstrace-controller-config";
export const STORAGE_KEY = "config.json";

export const isConfigStorage = (configMap: ConfigMap): boolean =>
  configMap.name === CONFIGMAP_NAME;

export const deserialize = (
  configMap: ConfigMap
): LatestControllerConfigType => {
  return LatestControllerConfigSchema.cast(
    JSON.parse(configMap.spec.data?.[STORAGE_KEY] ?? "")
  );
};

export const configmap = (kubeConfig: KubeConfiguration): ConfigMap => {
  return new ConfigMap(
    {
      apiVersion: "v1",
      kind: "ConfigMap",
      metadata: {
        name: CONFIGMAP_NAME
      },
      data: {
        [STORAGE_KEY]: "{}"
      }
    },
    kubeConfig
  );
};

export const serializeControllerConfig = (
  ccfg: LatestControllerConfigType,
  kubeConfig: KubeConfiguration
): ConfigMap => {
  const cm = new ConfigMap(
    {
      apiVersion: "v1",
      kind: "ConfigMap",
      metadata: {
        name: CONFIGMAP_NAME
      },
      data: {
        [STORAGE_KEY]: JSON.stringify(ccfg)
      }
    },
    kubeConfig
  );
  cm.setManagementOption({ protect: true }); // Protect so the reconciliation loop doesn't destroy it again.

  return cm;
};
