export class ConfigDoesNotExistError extends Error {
  constructor(...args: string[] | undefined[]) {
    super(...args);
    Error.captureStackTrace(this, ConfigDoesNotExistError);
  }
}
