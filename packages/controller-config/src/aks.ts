import { log, keyIDfromPEM, die } from "@opstrace/utils";

export function authenticatorKeySetAddKey(
  previousKeySetJSONString: string,
  newPubkeyPem: string
): string {
  const previousKeySet = JSON.parse(previousKeySetJSONString);
  const newKeyID = keyIDfromPEM(newPubkeyPem);

  if (newKeyID in previousKeySet) {
    die("key already in key set");
  }

  const newKeySet = {
    ...previousKeySet,
    [newKeyID]: newPubkeyPem
  };
  return JSON.stringify(newKeySet);
}

export function authenticatorKeySetRemoveKey(
  previousKeySetJSONString: string,
  keyIdToRemove: string
): string {
  const ks = JSON.parse(previousKeySetJSONString);

  if (!(keyIdToRemove in ks)) {
    die("key not present in key set");
  }

  delete ks[keyIdToRemove];
  return JSON.stringify(ks);
}

export function authenticatorKeySetgenerateJSONSingleKey(
  pubkeyPem: string
): string {
  // The key set is required to be a mapping between keyID (string) and
  // PEM-encoded pubkey (string).
  // Note: upon _continutation_, this key should be added to the existing
  // key set.
  const keyId = keyIDfromPEM(pubkeyPem);
  const keyset = {
    [keyId]: pubkeyPem
  };

  log.debug("built authenticator key ID: %s", keyId);

  // The corresponding configuration parameter value is expected to be a
  // string, namely the above `keyset` mapping in JSON-encoded form *without
  // literal newline chars*.
  const tenant_api_authenticator_pubkey_set_json = JSON.stringify(keyset);
  log.debug(
    "generated AuthenticatorPubkeySetJSON: %s",
    tenant_api_authenticator_pubkey_set_json
  );
  return tenant_api_authenticator_pubkey_set_json;
}
