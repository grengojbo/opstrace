import * as yup from "yup";
import { GCPConfig } from "@opstrace/gcp";
import { AWSConfig } from "@opstrace/aws";

// Between V1 and V2 there was a cluster launched that had the
// tenant_api_authenticator_pubkey_set_json field but before logRetention and
// metricRetention were renamed.
export const ControllerConfigSchemaV1alpha = yup
  .object({
    name: yup.string(),
    target: yup
      .mixed<"gcp" | "aws">()
      .oneOf(["gcp", "aws"])
      .required("must specify a target (gcp | aws)"),
    region: yup.string().required("must specify region"),
    logRetention: yup
      .number()
      .required("must specify log retention in number of days"),
    metricRetention: yup
      .number()
      .required("must specify metric retention in number of days"),
    dnsName: yup.string().required(),
    terminate: yup.bool().default(false),
    tenant_api_authenticator_pubkey_set_json: yup
      .string()
      .typeError()
      .strict(true),
    disable_data_api_authentication: yup.bool().required(),
    uiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    apiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    postgreSQLEndpoint: yup.string().notRequired(),
    opstraceDBName: yup.string().notRequired(),
    envLabel: yup.string(),
    // Note: remove one of cert_issuer and `tlsCertificateIssuer`.
    cert_issuer: yup
      .string()
      .oneOf(["letsencrypt-prod", "letsencrypt-staging"])
      .required(),
    tlsCertificateIssuer: yup
      .mixed<"letsencrypt-staging" | "letsencrypt-prod">()
      .oneOf(["letsencrypt-staging", "letsencrypt-prod"])
      .required(),
    infrastructureName: yup.string().required(),
    aws: yup.mixed<AWSConfig | undefined>(),
    gcp: yup.mixed<GCPConfig | undefined>(),
    controllerTerminated: yup.bool().default(false)
  })
  .noUnknown()
  .defined();

export type ControllerConfigTypeV1alpha = yup.InferType<
  typeof ControllerConfigSchemaV1alpha
>;
