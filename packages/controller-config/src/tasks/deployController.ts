import { all, AllEffect } from "redux-saga/effects";

import { KubeConfig } from "@kubernetes/client-node";
import { createResource, updateResource } from "@opstrace/kubernetes";
import { log } from "@opstrace/utils";

import { ControllerResources } from "../resources/controller";

export enum ControllerResourcesDeploymentStrategy {
  Create = 1,
  Update
}

export function* deployControllerResources(config: {
  controllerImage: string;
  opstraceClusterName: string;
  kubeConfig: KubeConfig;
  deploymentStrategy: ControllerResourcesDeploymentStrategy;
}): Generator<AllEffect<Promise<void>[]>, void, unknown> {
  let resources = ControllerResources(config)
    .get()
    .map(r => {
      // Protect these resources so we don't ever terminate them in the reconcile loop
      r.setManagementOption({ protect: true });
      return r;
    });

  switch (config.deploymentStrategy) {
    case ControllerResourcesDeploymentStrategy.Create: {
      log.debug("creating controller resources");
      yield all([resources.map(createResource)]);
      break;
    }

    case ControllerResourcesDeploymentStrategy.Update: {
      log.debug("updating controller resources");
      // Do not create a new secret when upgrading. This would create a mismatch between the secret
      // that is attached to the controller and the secret that is attached to hasura
      resources = resources.filter(r => r.name !== "hasura-admin-secret");

      yield all([resources.map(updateResource)]);
      break;
    }

    default: {
      throw new Error("invalid controller resource deployment strategy");
    }
  }
}
