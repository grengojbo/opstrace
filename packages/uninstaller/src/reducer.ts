import { combineReducers } from "redux";

import {
  statefulSetsReducer,
  persistentVolumesReducer,
  deploymentsReducer,
  daemonSetsReducer
} from "@opstrace/kubernetes";

export const rootReducers = {
  kubernetes: combineReducers({
    cluster: combineReducers({
      StatefulSets: statefulSetsReducer,
      Deployments: deploymentsReducer,
      DaemonSets: daemonSetsReducer,
      PersistentVolumes: persistentVolumesReducer
    })
  })
};

export const rootReducer = combineReducers(rootReducers);
export type State = ReturnType<typeof rootReducer>;
