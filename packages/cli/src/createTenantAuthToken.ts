import { log } from "@opstrace/utils";

import * as cli from "./index";
// import * as util from "./util";
import * as cryp from "./crypto";

export async function create(): Promise<void> {
  const keypair: cryp.RSAKeypair = cryp.readRSAKeyPairfromPEMfile(
    // Expect a PEM file encoding private key within `BEGIN RSA PRIVATE KEY ...
    // END RSA PRIVATE KEY` Note that this actually encodes the complete key
    // pair.
    // TODO: check file permissions, that this is 600-protected
    cli.CLIARGS.tenantApiAuthenticatorKeyFilePath
  );

  const token = cryp.generateJWTforTenantAPIfromKeyPair(
    cli.CLIARGS.tenantName,
    cli.CLIARGS.instanceName,
    keypair
  );

  log.info(
    "be sure that the Opstrace API authenticator has this public key in its key set:\n%s",
    keypair.pubkeyPem
  );
  process.stderr.write("\n");
  process.stdout.write(`${token}`);
  process.stderr.write("\n");
}
