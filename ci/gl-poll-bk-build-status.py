import os
import logging
import sys
import time

from pybuildkite.buildkite import Buildkite
from requests import RequestException


log = logging.getLogger()
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d %(levelname)s: %(message)s",
    datefmt="%y%m%d-%H:%M:%S",
)


BK_CLIENT = Buildkite()
BK_TOKEN = os.environ["BUILDKITE_API_TOKEN"].strip()
log.info("using BK token: %r....%r", BK_TOKEN[0], BK_TOKEN[-1])
BK_CLIENT.set_access_token(BK_TOKEN)

BK_ORG = "opstrace"
BK_PIPELINE = "prs"

# GitLab CI sets CI_COMMIT_SHA to "The commit revision the project is built
# for." (full git commit hash).
POLL_STATE_FOR_COMMIT = os.environ["CI_COMMIT_SHA"]

# Require any build to pop up for CI_COMMIT_SHA within this deadline. If no
# build is ever returned by the API within the deadline (or if the API doesn't
# work), exit with non-zero status code.
DEADLINE_BUILD_DISCOVERY_SECONDS = 60 * 15


def main():
    # Identify build to watch. Exit program if no build is found within
    # DEADLINE_BUILD_DISCOVERY_SECONDS.
    build = find_build_to_watch_or_terminate()
    bnumber = build["number"]
    burl = build["web_url"]

    # Watch build state forever. Terminate if build finishes. If build never
    # finishes expect that some outer timeout control eventually kills this
    # program.
    wait_seconds = 10
    log_every_n_seconds = 60
    t_last_logged = time.monotonic() - log_every_n_seconds
    log.info(
        "poll build status every %s s, log status every %s s",
        wait_seconds,
        log_every_n_seconds,
    )
    while True:
        b = get_build_status_with_retry(bnumber)
        if b is None:
            log.info("failed looking up build status, try again in ~10 s")
            time.sleep(10)
            continue

        state = b["state"]
        t1 = time.monotonic()
        if t1 - t_last_logged > log_every_n_seconds:
            t_last_logged = t1
            log.info("state: %s", state)

        if state in ("passed", "failed", "blocked", "canceled"):
            log.info("build finished: %s", state)

            if state == "passed":
                log.info("build %s passed: exit 0", burl)
                sys.exit(0)

            log.info("build %s did not pass: exit 1", burl)
            sys.exit(1)

        else:
            time.sleep(wait_seconds)


def get_builds_with_retry():
    log.info(
        "look up builds for commit %s in BK pipeline %s/%s",
        POLL_STATE_FOR_COMMIT,
        BK_ORG,
        BK_PIPELINE,
    )

    retry_deadline_seconds = 60 * 2
    t0 = time.monotonic()

    # Get builds for specific commit. Retry upon transient issues until
    # deadline is hit.
    while True:
        try:
            return BK_CLIENT.builds().list_all_for_pipeline(
                BK_ORG,
                BK_PIPELINE,
                commit=POLL_STATE_FOR_COMMIT,
                with_pagination=False,
            )
        except RequestException as exc:
            log.info("getting builds failed with: %s", str(exc))

            if hasattr(exc, "response"):
                if str(exc.response.status_code).startswith("4"):
                    log.info("4xx HTTP response: config issue? Terminate.")
                    sys.exit(1)

            # Assume that this is a transient problem (such as connection issue
            # or 5xx response). Retry
            if time.monotonic() - t0 > retry_deadline_seconds:
                log.info("local retry deadline hit -- treat as 0 builds found")
                return []

            log.info("try again in roughly 10 seconds")
            time.sleep(10)


def get_build_status_with_retry(bnumber):
    log.debug("look up status for build %s ", bnumber)
    retry_deadline_seconds = 60 * 2
    t0 = time.monotonic()

    # Retry upon transient issues until deadline is hit.
    while True:
        try:
            return BK_CLIENT.builds().get_build_by_number(BK_ORG, BK_PIPELINE, bnumber)
        except RequestException as exc:
            log.info("getting build status failed with: %s", str(exc))

            if hasattr(exc, "response"):
                if str(exc.response.status_code).startswith("4"):
                    log.info("4xx HTTP response: how did we get here? Terminate.")
                    sys.exit(1)

            # Assume that this is a transient problem (such as connection issue
            # or 5xx response). Retry
            if time.monotonic() - t0 > retry_deadline_seconds:
                log.info("local retry deadline hit -- return special signal None")
                return None

            log.info("try again in roughly 10 seconds")
            time.sleep(10)


def find_build_to_watch_or_terminate():
    t0 = time.monotonic()
    while True:
        builds = get_builds_with_retry()

        # Assume that `builds` is always a list, either empty or non-empty.
        # Docs are a little weak.. this should always be of type list.
        assert type(builds) == list, type(builds)

        if not builds:
            log.info("no builds seen yet for this commit.")
            if time.monotonic() - t0 > DEADLINE_BUILD_DISCOVERY_SECONDS:
                log.info("hit DEADLINE_BUILD_DISCOVERY_SECONDS. Exit non-zero.")
                sys.exit(1)

            log.info("try again in roughly 10 seconds")
            time.sleep(10)
            continue

        log.info("sorted by time (newest first):")
        for b in builds:
            log.info("   %s: created at %s", b["web_url"], b["created_at"])

        # Rely on sort order as of API docs: "Builds are listed in the order
        # they were created (newest first)." That is, this sort order is from
        # newer to older. Stop iteration when observing the first build that is
        # "too old".
        if len(builds) > 1:
            log.info(
                "newest build number / oldest build number: %s /%s",
                builds[0]["number"],
                builds[-1]["number"],
            )

        log.info("identified build to watch: %s", builds[0]["web_url"])
        return builds[0]


if __name__ == "__main__":
    main()
