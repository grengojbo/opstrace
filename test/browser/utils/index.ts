import winston from "winston";

export { performLogin, restoreLogin } from "./authentication";

const logFormat = winston.format.printf(
  ({ level, message, label, timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
  }
);

export const log = winston.createLogger({
  levels: winston.config.syslog.levels,
  level: "info",
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.timestamp(),
    winston.format.colorize(),
    logFormat
  ),
  transports: [new winston.transports.Console()]
});
