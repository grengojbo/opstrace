import { expect } from "@playwright/test";

import useFixtures from "../fixtures";
import { restoreLogin } from "../utils";

const test = useFixtures("auth");

test.describe("cluster health", () => {
  test.beforeEach(restoreLogin);

  test("all Cortext Ring Health tabs should render", async ({ page }) => {
    await page.hover("[data-test='sidebar/clusterAdmin/Health']");
    await page.click("[data-test='sidebar/clusterAdmin/Health']");
    await page.click("[data-test='sidebar/clusterAdmin/Health/System']");
    await page.click("[data-test='sidebar/clusterAdmin/Health/Metrics']");

    for (const tabName of [
      "Ingester",
      "Ruler",
      "Compactor",
      "Store-gateway",
      "Alertmanager"
    ]) {
      await page.click(`[data-test='ringHealth/tab/${tabName}']`);
      await page.waitForSelector(`[data-test='ringTable/shards']`);
      expect(
        await page.waitForSelector(`[data-test='ringTable/shards/row/ACTIVE']`)
      ).toBeTruthy();
    }
  });
});
