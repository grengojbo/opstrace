import { expect } from "@playwright/test";

import useFixtures from "../fixtures";
import { restoreLogin } from "../utils";

const test = useFixtures("auth");

test.describe("after auth0 authentication", () => {
  test.beforeEach(restoreLogin);

  test("user should see homepage, also OPSTRACE_PLAYWRIGHT_REUSE_STATE", async ({
    page,
    cluster
  }) => {
    expect(await page.isVisible("[data-test=getting-started]")).toBeTruthy();
  });

  test("user should see own email in user list", async ({ page, user }) => {
    await page.hover("[data-test='sidebar/tenant/Users']");
    await page.click("[data-test='sidebar/tenant/Users']");
    expect(
      await page.waitForSelector(`[data-test='user/row/${user.email}']`)
    ).toBeTruthy();
  });
});
