import { TestType } from "@playwright/test";

import { makeTenantName } from "../utils/tenant";

type TenantFixture = {
  newName: string;
};

export const addTenantFixture = (test: TestType) =>
  test.extend<
    Record<string, never>,
    {
      tenant: TenantFixture;
    }
  >({
    tenant: [
      async ({ browser }, use) => {
        await use({
          newName: makeTenantName()
        });
      },
      { scope: "test" }
    ]
  });
