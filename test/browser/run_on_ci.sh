#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o nounset
set -o pipefail


yarn pw:save-state-for-reuse

yarn playwright test --workers 3 --forbid-only --retries 3
