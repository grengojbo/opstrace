import { strict as assert } from "assert";
import fs from "fs";

import {
  ChannelCredentials,
  credentials,
  loadPackageDefinition,
  Metadata
} from "@grpc/grpc-js";
import { loadSync } from "@grpc/proto-loader";
import Long from "long";

import { OTLPTraceExporter } from "@opentelemetry/exporter-trace-otlp-grpc";
import { Resource } from "@opentelemetry/resources";
import { NodeTracerProvider } from "@opentelemetry/sdk-trace-node";
import { Span } from "@opentelemetry/sdk-trace-base";

import { Instant } from "@js-joda/core";
import {
  globalTestSuiteSetupOnce,
  log,
  readAuthTokenFile,
  rndstring,
  sleep,
  OPSTRACE_INSTANCE_DNS_NAME,
  TENANT_DEFAULT_API_TOKEN_FILEPATH
} from "./testutils";
import { PortForward } from "./testutils/portforward";

// Checks for the existence of a letsencrypt staging certificate.
// If one is found, adds it to the returned SSL options.
function getGrpcSSLOptions(caCertPath: string | undefined): ChannelCredentials {
  if (caCertPath !== undefined) {
    log.info(`using custom CA root cert: ${caCertPath}`);
    return credentials.createSsl(fs.readFileSync(caCertPath));
  } else {
    log.info(`using system CA root certs`);
    return credentials.createSsl();
  }
}

// Set up exporter that sends traces to default tenant
// We return the underlying OTLPTraceExporter to expose its send() call.
function getTenantExporter(
  tenantName: string,
  tenantTokenFile: string | undefined,
  caCertPath: string | undefined
): OTLPTraceExporter {
  const metadata = new Metadata();
  if (tenantTokenFile) {
    metadata.set(
      "Authorization",
      `Bearer ${readAuthTokenFile(tenantTokenFile)}`
    );
  }

  const collectorOptions = {
    url: `grpc://tracing.${tenantName}.${OPSTRACE_INSTANCE_DNS_NAME}:4317`,
    // If credentials isn't specified, plain http is used and we get back an error like this:
    // "13 INTERNAL: Received RST_STREAM with code 2 triggered by internal client error: Protocol error"
    credentials: getGrpcSSLOptions(caCertPath),
    metadata
  };
  return new OTLPTraceExporter(collectorOptions);
}

async function sendSpan(
  tenantName: string,
  tenantTokenFile: string | undefined,
  caCertPath: string | undefined,
  span: Span
) {
  const exporter = getTenantExporter(tenantName, tenantTokenFile, caCertPath);
  await new Promise((resolve, reject) =>
    exporter.send(
      [span],
      () => {
        log.info("sent span successfully");
        return resolve(null);
      },
      err =>
        reject(`failed to send span: message=${err.message} stack=${err.stack}`)
    )
  );
  await exporter.shutdown();
}

async function waitForMatchingSpans(
  jaegerNamespace: string,
  traceQuery: Record<string, unknown>,
  protoDescriptor: any
): Promise<boolean> {
  // We could query https://default.<OPSTRACE_HOST>/jaeger, but that requires Auth0 creds.
  // So instead use a port forward to reach the jaeger pod directly.
  const portForwardJaeger = new PortForward(
    "jaeger-default", // name (arbitrary/logging)
    "deployments/jaeger", // k8sobj
    16685, // grpc
    jaegerNamespace
  );
  const localPortJaeger = await portForwardJaeger.setup();

  try {
    const queryService = new protoDescriptor.jaeger.api_v2.QueryService(
      `127.0.0.1:${localPortJaeger}`,
      credentials.createInsecure()
    );

    // Wait up to ~30s for span to appear in queries
    // Normally should take <5s but it doesn't hurt to avoid flakes.
    for (const attempt of Array.from({ length: 30 }, (_v, k) => k)) {
      log.info(`querying for spans (attempt ${attempt})...`);
      const gotSpans: any[] = [];
      await new Promise((resolve, reject) => {
        const call = queryService.findTraces({ query: traceQuery });
        call.on("data", (data: any) => {
          // Avoid logging by default: Very noisy when checking e.g. cortex spans in system tenant
          log.debug(`findTraces data: ${JSON.stringify(data)}`);
          gotSpans.push(data["spans"]);
        });
        call.on("end", () => {
          log.info("findTraces end");
          resolve(null);
        });
        call.on("error", (e: any) => {
          reject(`findTraces error: ${JSON.stringify(e)}`);
        });
        call.on("status", (status: any) => {
          log.info(`findTraces status: ${JSON.stringify(status)}`);
        });
      });

      if (gotSpans.length > 0) {
        log.info("got spans, exiting loop");
        return true;
      }

      log.info("waiting 1s for span to appear");
      await sleep(1);
    }
    return false;
  } finally {
    await portForwardJaeger.terminate();
  }
}

suite("End-to-end tracing tests", function () {
  // Parse/generate query proto definition: reused across jaeger queries
  const jaegerPackageDefinition = loadSync("jaeger_proto/query.proto", {
    longs: String
  });
  const jaegerProtoDescriptor = loadPackageDefinition(
    jaegerPackageDefinition
  ) as any;

  suiteSetup(async function () {
    log.info("suite setup");
    globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    log.info("suite teardown");
  });

  test("Sending and reading default tenant spans", async function () {
    const spanLabel = rndstring(10);
    const serviceName = "test_otlp_tracing_api";
    const operationName = `test_${spanLabel}`;

    // Generate a basic span to send to the server
    const tracer = new NodeTracerProvider({
      resource: new Resource({
        "service.name": serviceName
      })
    }).getTracer("otel_library_name");
    const span = tracer.startSpan(operationName);
    span.setAttribute("testing", spanLabel);
    span.addEvent("emitting test span");
    span.end();

    const sentTimeSecs = Instant.now().toEpochMilli() / 1000;

    log.info(
      `sending span: service=${serviceName} operationName=${operationName}`
    );
    try {
      // Try with LE staging CA cert first - CI uses this
      await sendSpan(
        "default",
        TENANT_DEFAULT_API_TOKEN_FILEPATH,
        `${__dirname}/containers/letsencrypt-stg-root-x1.pem`,
        span as Span
      );
    } catch (e: any) {
      log.info(`sending with staging certs failed: ${e}`);
      // Fall back to system CA certs - manual clusters may be using letsencrypt-prod
      await sendSpan(
        "default",
        TENANT_DEFAULT_API_TOKEN_FILEPATH,
        undefined,
        span as Span
      );
    }

    // A very loose query range to avoid e.g. issues with drift on the test machine vs the server
    // Explicit conversion to Long is required, or else server fails to deserialize and returns an error like this:
    //   failed when searching for traces: stream error: rpc error: code = Unknown desc = start time is required for search queries
    const startTimeMinSecs = Long.fromNumber(sentTimeSecs - 3600);
    const startTimeMaxSecs = Long.fromNumber(sentTimeSecs + 3600);

    // This is a TraceQueryParameters, see jaeger_proto/query.proto
    const traceQuery = {
      serviceName,
      operationName,
      tags: {
        // Added to span via setAttribute() above.
        // Not strictly required since operationName is already unique, but doesn't hurt.
        testing: spanLabel
      },
      startTimeMin: {
        seconds: startTimeMinSecs
      },
      startTimeMax: {
        seconds: startTimeMaxSecs
      },
      // Must be explicitly non-zero or else we won't get any data.
      searchDepth: 1
    };

    const foundMatch = await waitForMatchingSpans(
      "default-tenant",
      traceQuery,
      jaegerProtoDescriptor
    );
    assert(foundMatch);
  });

  test("Reading system tenant spans", async function () {
    const sentTimeSecs = Instant.now().toEpochMilli() / 1000;
    // A very loose query range to avoid e.g. issues with drift on the test machine vs the server
    // Explicit conversion to Long is required, or else server fails to deserialize and returns an error like this:
    //   failed when searching for traces: stream error: rpc error: code = Unknown desc = start time is required for search queries
    const startTimeMinSecs = Long.fromNumber(sentTimeSecs - 3600);
    const startTimeMaxSecs = Long.fromNumber(sentTimeSecs + 3600);

    // This is a TraceQueryParameters, see jaeger_proto/query.proto
    const traceQuery = {
      serviceName: "cortex-ingester",
      startTimeMin: {
        seconds: startTimeMinSecs
      },
      startTimeMax: {
        seconds: startTimeMaxSecs
      },
      // Must be explicitly non-zero or else we won't get any data.
      searchDepth: 10
    };

    const foundMatch = await waitForMatchingSpans(
      "system-tenant",
      traceQuery,
      jaegerProtoDescriptor
    );
    assert(foundMatch);
  });
});
