// Force Typescript to behave on Object.entries https://github.com/microsoft/TypeScript/issues/21826#issuecomment-479851685
export const entries = Object.entries as <T>(
  o: T
) => [Extract<keyof T, string>, T[keyof T]][];

export interface Array<T> {
  filter<S>(
    callbackfn: (this: void, value: T, index: number, array: T[]) => boolean
  ): S[];
}
