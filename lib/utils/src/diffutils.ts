import * as Diff from "diff";
import { log, isDebugLogging } from "./log";

/**
 * Deep diff between two objects
 */
export function diff(
  old: any, // eslint-disable-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  possiblyChangedObj: any // eslint-disable-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) {
  return Diff.diffJson(old, possiblyChangedObj);
}

export function logDiff(
  old: any, // eslint-disable-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  possiblyChangedObj: any // eslint-disable-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
) {
  if (!isDebugLogging()) {
    // Avoid calculating a diff string if debug isn't enabled
    return;
  }
  let msg = "";
  diff(old, possiblyChangedObj).forEach(change => {
    if (change.added) {
      msg += `\x1b[32m${change.value}\x1b[0m`;
    } else if (change.removed) {
      msg += `\x1b[31m${change.value}\x1b[0m`;
    } else {
      msg += `\x1b[2m${change.value}\x1b[0m`;
    }
  });
  if (msg.length) {
    log.debug(msg);
  }
}
