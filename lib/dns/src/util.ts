export const constructNSRecordOptions = ({
  name,
  nameservers
}: {
  name: string;
  nameservers: string[];
}): {
  name: string;
  data: string[];
  ttl: number;
} => ({
  name,
  data: nameservers,
  ttl: 30
});

export const getSubdomain = ({
  stackName,
  dnsName
}: {
  stackName: string;
  dnsName: string;
}): string => `${stackName}.${dnsName}`;
