import { K8sResource } from "./common";
import { kubernetesError } from "./utils";
import { log } from "@opstrace/utils";

const dryRunKubeApiWrites = process.env.DRY_RUN_KUBERNETES_API_WRITES
  ? true
  : false;

export const createResource = async (resource: K8sResource): Promise<void> => {
  log.debug(
    `createResource for %s(ns: %s) [dryrun=${dryRunKubeApiWrites}]`,
    resource.name,
    resource.namespace
  );
  if (dryRunKubeApiWrites) {
    return;
  }
  try {
    await resource.create();
  } catch (e: any) {
    const err = kubernetesError(e);
    if (err.statusCode !== 409) {
      log.info(
        "api err during createResource for %s(ns: %s): %s",
        resource.name,
        resource.namespace,
        err.message
      );
    } else {
      log.debug("createResource(): ignored 409 error");
    }
  }
  return;
};

export const deleteResource = async (resource: K8sResource): Promise<void> => {
  log.debug(
    `deleteResource for %s(ns: %s) [dryrun=${dryRunKubeApiWrites}]`,
    resource.name,
    resource.namespace
  );
  if (dryRunKubeApiWrites) {
    return;
  }
  try {
    await resource.delete();
  } catch (e: any) {
    const err = kubernetesError(e);
    if (err.statusCode !== 404) {
      log.info(
        "api err during deleteResource for %s(ns: %s): %s",
        resource.name,
        resource.namespace,
        err.message
      );
    } else {
      log.debug("deleteResource(): ignored 404 error");
    }
  }
  return;
};

export const updateResource = async (resource: K8sResource): Promise<void> => {
  log.debug(
    `updateResource for %s %s/%s [dryrun=${dryRunKubeApiWrites}]`,
    resource.kind,
    resource.namespace,
    resource.name
  );
  if (dryRunKubeApiWrites) {
    return;
  }
  try {
    await resource.update();
  } catch (e: any) {
    const err = kubernetesError(e);
    log.info(
      "api err during updateResource for %s %s/%s: %s: %s",
      resource.kind,
      resource.namespace,
      resource.name,
      err.message,
      e
    );
  }
  return;
};
