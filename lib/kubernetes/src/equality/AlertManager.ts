import { isDeepStrictEqual } from "util";
import { V1Alertmanager } from "..";

export const isAlertManagerEqual = (
  desired: V1Alertmanager,
  existing: V1Alertmanager
): boolean => {
  if (!isDeepStrictEqual(desired.spec, existing.spec)) {
    return false;
  }

  return true;
};
