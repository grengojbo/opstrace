import { isDeepStrictEqual } from "util";
import { V1Prometheus } from "..";

export const isPrometheusEqual = (
  desired: V1Prometheus,
  existing: V1Prometheus
): boolean => {
  if (!isDeepStrictEqual(desired.spec, existing.spec)) {
    return false;
  }

  return true;
};
