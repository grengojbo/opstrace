import { RDS } from "aws-sdk";
import { log } from "@opstrace/utils";

import { rdsClient, awsPromErrFilter } from "./util";
import { AWSApiError } from "./types";
import { AWSResource } from "./resource";

class RDSInstanceRes extends AWSResource<
  RDS.DBInstance,
  RDS.CreateDBInstanceMessage
> {
  protected rname = "RDS Aurora instance";

  protected async tryCreate(params: RDS.CreateDBInstanceMessage) {
    const result: RDS.CreateDBInstanceResult = await awsPromErrFilter(
      rdsClient().createDBInstance(params).promise()
    );

    if (result && result.DBInstance) {
      return true;
    }
    return false;
  }

  protected async checkCreateSuccess(): Promise<RDS.DBInstance | false> {
    // use legacy `doesRDSInstanceExist()` function here, it has the right
    // logic with respect to the opstrace_cluster_name tag.
    const instance = await doesRDSInstanceExist({
      opstraceDBInstanceName: this.ocname
    });

    if (instance === false) {
      return false;
    }

    log.info("RDS instance status: %s", instance.DBInstanceStatus);

    if (instance.DBInstanceStatus === "available") {
      return instance;
    }

    if (instance.DBInstanceStatus?.toLowerCase() === "failed") {
      log.info(
        "Bad luck: RDS instance creation failed (rare AWS-internal problem)"
      );
      log.info(
        "Tearing down FAILED RDS instance, then creating a new one (same name)"
      );

      // The `FAILED` RDS instance needs to be destroyed before we can retry
      // creating one with the same name as before.
      await this.teardown();

      // Issue another CREATE API call in the next setup() iteration.
      this.resetCreationState();
    }

    return false;
  }

  protected async tryDestroy() {
    await awsPromErrFilter(
      rdsClient()
        .deleteDBInstance({
          DBInstanceIdentifier: this.ocname,
          // Skip final snapshotting of the DB before destroy - we don't care about keeping a snap
          SkipFinalSnapshot: true
        })
        .promise()
    );
  }

  protected async checkDestroySuccess(): Promise<true | string> {
    const instance = await doesRDSInstanceExist({
      opstraceDBInstanceName: this.ocname
    });

    if (instance === false) {
      return true;
    }

    log.info("RDS instance status: %s", instance.DBInstanceStatus);

    // string representing state
    return JSON.stringify(instance, null, 2);
  }
}

async function getInstance(
  dbInstanceIdentifier: string
): Promise<RDS.DBInstance | false> {
  let result: RDS.DBInstanceMessage;
  try {
    result = await awsPromErrFilter(
      rdsClient()
        .describeDBInstances({ DBInstanceIdentifier: dbInstanceIdentifier })
        .promise()
    );
    if (result && result.DBInstances && result.DBInstances.length > 0) {
      return result.DBInstances[0];
    }
    return false;
  } catch (e: any) {
    if (e instanceof AWSApiError) {
      // well-defined, explicit confirmation that instance does not exist.
      if (e.name == "DBInstanceNotFound") {
        return false;
      }
    }

    if (e.name == "InvalidParameterValue") {
      // The lookup may surprisingly be responded to with a 'bad request'
      // response when the lookup identifier does not comply with naming rules.
      // Example log message: info: RDS Aurora instance teardown:
      // checkDestroy(): aws api error: "InvalidParameterValue: The parameter
      // Filter: db-instance-id is not a valid identifier. Identifiers must
      // begin with a letter; must contain only ASCII letters, digits, and
      // hyphens; and must not end with a hyphen or contain two consecutive
      // hyphens. (HTTP status code: 400)"
      // treat this as "does not exist", because it's known that this RDS
      // instance cannot exist -- it was never created in the first place
      // as of the bad name.
      return false;
    }

    throw e;
  }
}

/**
 * Check if RDS instance exists for specific Opstrace instance, matched via name
 * as well as opstrace_cluster_name resource tag. Return RDS.DBInstance type if
 * yes or false if no.
 */
export async function doesRDSInstanceExist({
  opstraceDBInstanceName
}: {
  opstraceDBInstanceName: string;
}): Promise<RDS.DBInstance | false> {
  // see if there is an RDS instance with the RDS instance name matching the
  // Opstrace instance name.
  const instance: RDS.DBInstance | false = await getInstance(
    opstraceDBInstanceName
  );

  if (instance) {
    return instance;
  }

  return false;
}

export async function ensureRDSInstanceExists({
  opstraceDBInstanceName,
  instanceLabels
}: {
  opstraceDBInstanceName: string;
  instanceLabels: { Key: string; Value: string }[];
}): Promise<RDS.DBInstance> {
  const dbCreateParams: RDS.CreateDBInstanceMessage = {
    // current convention: DBInstanceIdentifier matches opstrace cluster name
    DBInstanceIdentifier: opstraceDBInstanceName,
    DBClusterIdentifier: opstraceDBInstanceName,
    Engine: "aurora-postgresql",
    DBInstanceClass: "db.t3.medium",
    Tags: instanceLabels
  };

  return await new RDSInstanceRes(opstraceDBInstanceName).setup(dbCreateParams);
}

export async function destroyRDSInstance(
  opstraceDBInstanceName: string
): Promise<void> {
  return await new RDSInstanceRes(opstraceDBInstanceName).teardown();
}
