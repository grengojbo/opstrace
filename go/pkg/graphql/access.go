package graphql

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type GraphqlAccess struct {
	URL    string
	client *http.Client
	secret string
}

func NewGraphqlAccess(url *url.URL, secret string) *GraphqlAccess {
	return &GraphqlAccess{
		url.String(),
		&http.Client{},
		secret,
	}
}

// Execute adds the secret onto the request, executes it, and deserializes the response into `result`.
func (g *GraphqlAccess) Execute(req *http.Request, result interface{}) error {
	if g.secret != "" {
		req.Header.Add("x-hasura-admin-secret", g.secret)
	}

	resp, err := execute(g.client, req)
	if err != nil {
		return err
	}

	if len(resp.Errors) != 0 {
		return fmt.Errorf("query to %s failed: %s", req.URL.Path, resp.Error())
	}

	if err := json.Unmarshal(resp.Data, result); err != nil {
		return fmt.Errorf("failed to unmarshal response JSON '%s': %s", resp.Data, err)
	}

	return nil
}
