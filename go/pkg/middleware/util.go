package middleware

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// Read all response body bytes, and return response body as string, with
// leading and trailing whitespace stripped.
func GetStrippedBody(resp *http.Response) string {
	rbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(fmt.Errorf("readAll error: %v", err))
	}
	return strings.TrimSpace(string(rbody))
}
