package opstraceauthextension

import "go.opentelemetry.io/collector/config"

type Config struct {
	config.ExtensionSettings `mapstructure:",squash"`

	TenantName string `mapstructure:"tenantname"`
}
