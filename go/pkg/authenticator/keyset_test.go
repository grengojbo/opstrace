package authenticator

import (
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestKeysetFromEnv_TwoKeys(t *testing.T) {
	os.Setenv("API_AUTHTOKEN_VERIFICATION_PUBKEY_SET", TestKeysetEnvValThreePubkeys)

	log.Infof("keyset map:\n%v", authtokenVerificationPubKeys)
	log.Infof("fallback key:\n%v", authtokenVerificationPubKeyFallback)

	assert.Empty(
		t,
		authtokenVerificationPubKeys,
		"map authtokenVerificationPubKeys expected to be empty",
	)
	readKeySetJSONFromEnvOrCrash()

	log.Infof("keyset map:\n%v", authtokenVerificationPubKeys)
	log.Infof("fallback key:\n%v", authtokenVerificationPubKeyFallback)

	assert.NotEmpty(
		t,
		authtokenVerificationPubKeys,
		"map authtokenVerificationPubKeys expected to not be empty",
	)
}

func TestKeysetFromEnv_EmtpySetButFallback(t *testing.T) {
	os.Setenv("API_AUTHTOKEN_VERIFICATION_PUBKEY_SET", "")
	os.Setenv("API_AUTHTOKEN_VERIFICATION_PUBKEY", TestPubKey)

	// This is now expected to _not_ crash, becuse a fallback key is
	// configured.
	ReadConfigFromEnvOrCrash()
	log.Infof("keyset map:\n%v", authtokenVerificationPubKeys)
	log.Infof("fallback key:\n%v", authtokenVerificationPubKeyFallback)
}
